

#     Copyright (C) 2015  Markus Mayrhofer, Björn Viklund
#
#     This source code is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; version 2.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     Some data contained in this R-package are derived from files
#     provided by Affymetrix (.cdf, .cn.annot.csv and .annot.csv),
#     and which are copyrighted by Affymetrix.


# ____
#|  _ \ __ ___      _____ ___  _ __  _   _
#| |_) / _` \ \ /\ / / __/ _ \| '_ \| | | |
#|  _ < (_| |\ V  V / (_| (_) | |_) | |_| |
#|_| \_\__,_| \_/\_/ \___\___/| .__/ \__, |
#                             |_|    |___/

##' Takes a set of CEL files of Affymetrix CytoScanHD, CytoScan750k or SNP 6.0 and produces normalized log-ratio,
##' B-allele ratio and optional segmentation for downstream analysis.
##'
##' @param CELfiles.or.directory A vector of CEL file names, or a directory containing the CEL files to be run. Default is the working directory.
##' @param referenceFile Optional reference file produced with rawcopy.makeReference(). May further reduce noise and waviness.
##' @param outdir Directory where results are to be stored. One subdirectory will be created per sample. Default is the working directory.
##' @param cores Sets number of cores to use for parallel processing of samples.
##' @param segmentation Whether to produce segment table files for each sample. Currently single-threaded.
##' @param segment.alpha Sets the segmentation significance threshold. A lower value will result in fewer segments.
##' @param seed Sets the segmentation and allelic imbalance seed.
##' @param writeData Whether to write log-ratio and B-allele ratio to text files.
##' @param writeSummary Whether to summarize sample data in a table file.
##' @param allSNPs Whether to report B-allele ratio for SNPs of poor quality (based on reference material only).
##' @param resume Whether to use previously processed data (automatically stored in '[sample]/rawcopy.Rdata').
##' @param verbose verbose output (for debugging)
##'
##' @details
##' Output files:
##' \itemize{
##'  \item{"probes.txt"}{ (log-ratio): Contains DNA abundance estimates (log2 of sample intensity relative to a pool of diploid references) for all markers. Also includes raw intensity and B-allele ratio (where available).}
##'  \item{"snps.txt"}{ (B-allele ratio): Contains B-allele ratio, raw intensities and alleles for all SNP markers.}
##'  \item{"segments.txt /xlsx"}{ (segments): Optional, contains estimated genomic segments with the same set of copy numbers in the cell population.}
##'  \item{"qc.txt"}{: Sample statistics.}
##'  \item{"overview.[sample name].png"}{: Sample overview figure with some statistics.}
##'  \item{"rawcopy.Rdata"}{: Data stored for segmentation, plotting and more.}
##'  \item{"snp.profile.Rdata"}{: Set of SNP allele ratios for detection of related or patient-matched samples.}
##' }
##'
##' @author Markus Mayrhofer, Björn Viklund
##' @rdname rawcopy.rawcopy
##' @aliases rawcopy
##' @export
##' @import affxparser
##' @import foreach
##' @import PSCBS
##' @import squash

rawcopy <- function(CELfiles.or.directory=getwd(),
                    referenceFile=NULL,
                    wavinessFile=NULL,
                    outdir=getwd(),
                    cores=1,
                    segmentation=TRUE,
                    writeData=FALSE,
                    writeSummary=TRUE,
                    allSNPs=FALSE,
                    resume=FALSE,
                    segment.type='PSCBS',
                    segment.alpha=0.005,
                    seed=NULL,
                    MDS=TRUE,
                    verbose=FALSE) {


    if (length(grep('\\.CEL$',CELfiles.or.directory))==0) {
        CELfiles=dir(path=CELfiles.or.directory)[grep('\\.CEL$',dir(path=CELfiles.or.directory))]
        CELfiles=paste(CELfiles.or.directory,CELfiles,sep='/')
    } else {
        CELfiles=CELfiles.or.directory#[grep('.CEL',celfiles.or.directory)]
    } #CELfiles are now the files to run, with full or relative path.

    suppressPackageStartupMessages(library(affxparser))
    suppressPackageStartupMessages(library(foreach))
    suppressPackageStartupMessages(library(PSCBS))

    suppressWarnings(
        try( {
            suppressPackageStartupMessages(library(doMC))
            registerDoMC(cores=cores)
        }, silent=T
        )
    )

    cat(paste(c('Selected samples:',CELfiles),collapse='\n'),'\n\n')
    cat('Results in:\n',outdir,'\n\n')
    dir.create(outdir,showWarnings=F)

    n <- length(CELfiles)
    names <- outdirs <- NULL
    for (i in 1:n) {
        name=strsplit(x=as.character(CELfiles[i]),split='/')[[1]]
        names[i]=strtrim(name[length(name)],nchar(name[length(name)])-4)
        outdirs[i]=paste(outdir,names[i],sep='/')
    }
    #browser()
    celfile=readCel(as.character(CELfiles[1]))
    array=celfile$header$chiptype


    cat(format(Sys.time(), "%F %H:%M:%S"),'Loading',array,'data \n')
    #Check if the arraytype is SNP6
    if(array == 'GenomeWideSNP_6') {
        data('SNP6_annot',package='rawcopy')
        data('SNP6_cdf',package='rawcopy')
        data('SNP6_pannot',package='rawcopy')
        data('SNP6_probeClusters',package='rawcopy')
        data('SNP6_snpClusters',package='rawcopy')
        data('SNP6_mds',package='rawcopy')
    }

    #Check if the arraytype is CytoScanHD
    if(array == 'CytoScanHD_Array') {
        data('CytoScanHD_annot',package='rawcopy')
        data('CytoScanHD_cdf',package='rawcopy')
        data('CytoScanHD_pannot',package='rawcopy')
        data('CytoScanHD_probeClusters',package='rawcopy')
        data('CytoScanHD_snpClusters',package='rawcopy')
        data('CytoScanHD_mds',package='rawcopy')
    }

    #Check if the arraytype is 750K. Uses CytoScanHD reference data.
    if(array == 'CytoScan750K_Array') {
        data('CytoScan750K_cdf',package='rawcopy')
        data('CytoScanHD_annot',package='rawcopy')
        data('CytoScanHD_pannot',package='rawcopy')
        data('CytoScanHD_probeClusters',package='rawcopy')
        data('CytoScanHD_snpClusters',package='rawcopy')
    }

    cat(format(Sys.time(), "%F %H:%M:%S"),'Analyzing',n,'CEL files\n')
    # for (i in 1:n) { #(forEach)
    foreach(i = 1:n, .verbose=verbose) %dopar% {

        celfile=readCel(as.character(CELfiles[i]))
        name=strsplit(x=as.character(CELfiles[i]),split='/')[[1]]
        name=strtrim(name[length(name)],nchar(name[length(name)])-4)
        this.outdir=paste(outdir,name,sep='/')
        try( dir.create(path=this.outdir,showWarnings=F))

        ## Run the sample
        s <- 0
        if (!resume | !file.exists(paste(this.outdir,'rawcopy.Rdata',sep='/'))) {
            s <- system.time(
                rawcopy.affy1(name, celfile,
                              referenceFile,
                              wavinessFile,
                              this.outdir,
                              cdf,
                              annot, pannot,
                              snpClusters, probeClusters,
                              writeData=writeData,
                              allSNPs=allSNPs,
                              MDS=MDS
                )
            )
        }


        if (writeData) {
            #Load rawcopy.Rdata and save probes.txt to a text file
            load(paste(this.outdir,'rawcopy.Rdata',sep='/'))
            #Create Nexus format
            #Remove end
            nex <- probes.txt
            nex <- nex[,-which(names(probes.txt) == 'End')]
            #Modify column names
            names(nex)[names(nex) == 'Chromosome'] <- 'Chromosome'
            names(nex)[names(nex) == 'Start'] <- 'Position'
            names(nex)[names(nex) == 'Value'] <- "Log R Ratio"
            names(nex)[names(nex) == 'B.Allele.Frequency'] <- 'B-Allele Frequency'
            write.table(nex[!is.na(nex$Chromosome),],file=paste(this.outdir,'/',name,'.txt',sep=''),quote=F,row.names=F,sep='\t',na=' ')
            rm(nex)
        }


        if (is.na(floor(s[3]))) s=c(0,0,0)
        cat(format(Sys.time(), "%F %H:%M:%S"),' ',i,' processed out of ',n,' (',floor(s[3]/60), ' min)\n',sep='')
    }
    # browser()
    # Visualize and summarize the batch
    if (writeSummary) suppressWarnings(try( {
        dists <- rawcopy.summary(outdir)
    }, silent=T ))
    # Segment each subset to be co-segmented:
    segix=rep(NA,length(CELfiles))
    if (is.logical(segmentation) & segmentation[1]==TRUE) {
        # Segment all separately
        segix=1:length(CELfiles); segix[!segmentation]=NA
    }
    if (is.vector(segmentation) & is.numeric(segmentation)) {
        ## A number vector tells which samples go together
        segix=segmentation
    }
    if (segmentation[1]=='automatic') {
        ## Use dists to decide on co-segmentation
    }
    if (segmentation[1]=='matched') {
        segix=rep(1,n)
    }
    ## Now segment each set of samples to be co-segmented
    #browser()
    #a <- for(i in unique(segix)) {
    #browser()
    a <- foreach(i = unique(segix), .verbose=verbose) %dopar%{
        if (!is.na(i)) {
            ix=segix==i
            ix[is.na(ix)]=F
            cat(format(Sys.time(), "%F %H:%M:%S"),'Segmentation: ',paste(names[ix],collapse=',\n'),'\n')
            segment.wrapper(names[ix],outdirs[ix],plot=F,method=segment.type,alpha=segment.alpha,seed=seed)
        }
    }

    ## Plot QC plots for each sample
    # ____  _       _    ___   ____
    #|  _ \| | ___ | |_ / _ \ / ___|
    #| |_) | |/ _ \| __| | | | |
    #|  __/| | (_) | |_| |_| | |___
    #|_|   |_|\___/ \__|\__\_\\____|
    #
    #PlotQC

    # for (i in 1:n) {
    try(dev.off(),silent=T)
    try(dev.off(),silent=T)
    try(dev.off(),silent=T)
    try(dev.off(),silent=T)
    a <- foreach (i = 1:n, .verbose=verbose) %dopar% {
        name=names[i]
        this.outdir=outdirs[i]
        cat(format(Sys.time(), "%F %H:%M:%S"),'Plotting:',name,'\n')
        segments.txt=NULL
        if (file.exists(paste(this.outdir,'segments.txt',sep='/')))
            segments.txt=load.txt(paste(this.outdir,'segments.txt',sep='/'))
        load(paste(this.outdir,'rawcopy.Rdata',sep='/'))
        plotQc(segments.txt,snps.txt,probes.txt,lr,raf,taps.plot,qc,name,this.outdir,intensities)
        plotChromosomeQc(segments.txt,snps.txt,probes.txt,lr,raf,taps.plot,qc,name,this.outdir,intensities)
        file.copy(paste(this.outdir,paste('qc',name,'png',sep='.'),sep='/'),
                  paste(outdir,paste('qc',name,'png',sep='.'),sep='/'),
                  overwrite=T)
        return(NULL)
    }
    cat(format(Sys.time(), "%F %H:%M:%S"),'Finished.','\n')
}



# ____
#/ ___| _   _ _ __ ___  _ __ ___   __ _ _ __ _   _
#\___ \| | | | '_ ` _ \| '_ ` _ \ / _` | '__| | | |
# ___) | |_| | | | | | | | | | | | (_| | |  | |_| |
#|____/ \__,_|_| |_| |_|_| |_| |_|\__,_|_|   \__, |
#                                            |___/
#Summary

##' Text about summary
##'
##' @param dir Directory.
##' @author Markus Mayrhofer, Björn Viklund
##' @export
##' @import squash
##' @import ape
##' @import SDMTools
rawcopy.summary <- function(dir=getwd()) {

    clust <- NULL

    suppressPackageStartupMessages(library(squash))
    suppressPackageStartupMessages(library(ape))
    suppressPackageStartupMessages(library(SDMTools))

    # setwd('/media/safe/bjorn/2014/test')
    # setwd('/media/safe/bjorn/2014/PA150')
    # setwd('/media/safe/bjorn/2014/PA242')
    # outdir=getwd()
    # setwd(dir)
    #browser()
    sampleData=data.frame()
    profiles=data.frame()
    allSegments <- data.frame()
    clust=NULL
    ## hitta vilka mappar ska köras på först
    #...
    #You could use list.dirs. But you'll get the current directory '.' and the code would look uglier
    dirs=dir(dir,recursive=T)
    dirs=dirs[grep('rawcopy.Rdata',dirs)]
    samples=sub(pattern='/rawcopy.Rdata','',dirs)

    #samples <- list.files(dir)[file.info(list.files(dir))$isdir]
    # profileList <- lapply(1:length(samples),function(i) i)
    profileList <- NULL# lapply(1:length(grep('snp.profile.Rdata',list.files(dir,recursive=T))),function(i) i)
    shortsegList <- NULL
    tableList <- NULL
    segmentList <- NULL


    # col <- rainbow(length(samples),alpha=1)
    listIndex <- 1
    for(i in 1:length(samples)) {
        sampDir <- paste(dir,samples[i],sep='/')
        #browser()

        if(file.exists(paste(sampDir,'qc.txt',sep='/')) ) {

            #cat(i,'\n')
            #get the QC
            # qc=load.txt(paste(sampDir,'qc.txt',sep='/'))
            qc <- read.csv(paste(sampDir,'qc.txt',sep='/'),header=T,sep='\t')
            # sampleData=rbind(sampleData,qc)
            try(sampleData <- rbind(sampleData,qc),silent=T)
        }

        # cat(listIndex,'\n')
        if( file.exists(paste(sampDir,'snp.profile.Rdata',sep='/')) & file.exists(paste(sampDir,'qc.txt',sep='/')) ) {
            #get the SNPs for matching
            load(paste(sampDir,'snp.profile.Rdata',sep='/'))
            profileList[[listIndex]] <- snp.profile
        }
        if( file.exists(paste(sampDir,'cna.profile.Rdata',sep='/')) & file.exists(paste(sampDir,'qc.txt',sep='/')) ) {
            #get the copy number alteration profile
            load(paste(sampDir,'cna.profile.Rdata',sep='/'))
            table=cna.profile$table
            table$imba[is.na(table$imba)]=0.1
            ix=deChrom_ucsc(table$chr) < 23
            ai=table$imba[ix]
            lr=c(table$avlog-table$armMedian,table$avlog[ix]) # total for autosomes and arm-relative for all
            lr[is.na(lr)]=0
            lr[lr>0.07]=1
            #lr[lr>0.2]=2
            lr[lr< -0.1]= -1
            #lr[lr< -0.3]= -2

            ai[is.na(ai)]=0.1
            ai[ai>0.4]=1
            ai[ai<=0.4 | table$avlog[ix]>0.01]=0
            ai=NULL # removed it for now

            shortsegList[[listIndex]] <- c(as.numeric(lr),ai)
            tableList[[listIndex]] <- table
        }
        if( file.exists(paste(sampDir,'segments.txt',sep='/')) & file.exists(paste(sampDir,'qc.txt',sep='/')) ) {
            #get the segments
            s <- load.txt(paste(sampDir,'segments.txt',sep='/'))
            segmentList[[listIndex]] <- s
        }
        listIndex <- listIndex + 1
    }

    #Save sampleData as xlsx and csv
    write.table(sampleData,file=paste(dir,'QC.csv',sep='/'),row.names=F,sep='\t')
    #write.xlsx(sampleData,file=paste(dir,'QC.xlsx',sep='/'),row.names=F)

    if(length(profileList) > 1) {
        #Distogram
        #New order very fine!
        # browser()

        profiles <- do.call('cbind',profileList)
        colnames(profiles) <- strtrim(colnames(profiles),70)
        smallProfiles <- profiles[1:round(nrow(profiles) * 0.9),]
        logProfiles <- smallProfiles < 0.2

        preClust <- dist(t(logProfiles[seq(1,ceiling(nrow(logProfiles)*.9),by=5e1),]))

        h <- hclust(preClust)
        order <- h$order

        preClust <- dist(t(logProfiles[seq(1,ceiling(nrow(logProfiles)*.9),by=5e1),order]))
        matrix <- as.matrix(preClust)
        diag(matrix) <- NA
        frameDist <- as.data.frame(matrix)
        skipList <- NULL
        nameOrder <- h$labels[order]
        order2 <- order
        close <- 40
        for(i in 1:(nrow(frameDist))) {
            curName <- rownames(frameDist[i,])
            if(curName %in% skipList) next()

            if(sum(as.numeric(frameDist[i,]) < close,na.rm=T) < 2) {
                next()
            }

            relatives <- names(frameDist[i,which(as.numeric(frameDist[i,]) < close & !is.na(frameDist[i,]))])
            for(rel in relatives[rev(order(frameDist[i,relatives]))]) {
                it <- which(relatives == rel)
                curIx <- which(nameOrder == curName)
                moveIx <- which(nameOrder == rel)
                if(abs(curIx-moveIx) == 1) next()

                newOrder <- 1:nrow(frameDist)
                newOrder <- append(newOrder[-moveIx],moveIx,after=curIx-1)

                order2 <- order2[newOrder]
                nameOrder <- nameOrder[newOrder]
                frameDist <- frameDist[newOrder,newOrder]
            }
        }

        order <- order2
        # clust <- dist(t(logProfiles[,order[100:250]]))
        clust <- dist(t(logProfiles[,order]))
        # pic()


        # if(ncol(profiles) < 10) {
        #     width <- height <- 9
        # }
        textScale <- 1
        if(ncol(profiles) < 20) {
            width <- height <- 9
        }
        if(ncol(profiles) >= 20 & ncol(profiles) <= 70) {
            width <- height <- 11
        }
        if(ncol(profiles) > 70) {
            width <- height <- ncol(profiles) * 0.16
            textScale <- 1 + (ncol(profiles)) / 200
        }

        # browser()
        pdf(file=paste(dir,'/sampleIdentityDistogram_',format(Sys.time(), "%F-%H_%M_%S"),'.pdf',sep=''),width=width,height=height)
        par(mar=c(0.5,0.5,0.5,0.5))
        try({
            #Draw distogram
            clustOri <- clust
            clust <- (clust^2)/nrow(logProfiles)
            #kolla nivåer
            # hist((clust^2)/nrow(logProfiles),1000)
            # hist((clust)/nrow(logProfiles),1000)
            map <- makecmap(seq(0,.5,by=0.01),500,colFn=colorFunction)
            a <- distogram(clust,map,key=F)

            #Create legend
            par(new=T)
            plot(1,type='n',xlim=c(0,1),ylim=c(0,1),axes=F)

            nsamples <- ncol(logProfiles)
            #X pos
            X <- 0.006
            #x offset to print text
            xdiff <- 0.03
            x <- c(-nsamples/2+xdiff,-nsamples/2,-nsamples/2,-nsamples/2+xdiff)
            x <- c(X+xdiff,X,X,X+xdiff)

            #Y pos
            yheight <- 0.18
            y <- c(0,0,yheight,yheight)
            pnts <- cbind(x,y)
            # legend.gradient(pnts,map$colors[1:100],c("Low","Medium",'High','Extreme'))
            legend.gradient(pnts,map$colors,limits=c('',''),title='')

            segments(x0=c(X,X,X+xdiff,X+xdiff),
                     x1=c(X,X+xdiff,X+xdiff,X),
                     y0=c(0,yheight,yheight,0),
                     y1=c(yheight,yheight,0,0))

            #Text that correlates to the legend
            maxcol <- 0.4
            title <- (0.43 * yheight) / maxcol
            black <- (0 * yheight ) / maxcol
            red <- (0.17 * yheight ) / maxcol
            yellow <- (0.3 * yheight ) / maxcol
            white <- (0.37 * yheight ) / maxcol
            white2 <- (.4 * yheight ) / maxcol
            meanWhite <- (white + white2) / 2
            ypoint <- c(black,red,yellow,white2)
            xpoint <- rep(X+0.1,length(ypoint))

            #Ugly but works
            labelsText <- c('  Abnormally different',
                        '  Unmatched',
                        '  Parent/sibling level match',
                        '  Patient identity level match')
            labelsPerc <- c('0-70%','77%','86%','100%')
            xLen <- length(xpoint)
            opar <- par()
            par(xpd=T)
            text(xpoint[1],ypoint[1],labels=rev(labelsText)[1],adj=c(0,0),cex=textScale)
            text(xpoint[1],ypoint[1],labels=rev(labelsPerc)[1],adj=c(1,0),cex=textScale)

            text(xpoint[xLen],ypoint[xLen],labels=rev(labelsText)[xLen],adj=c(0,1),cex=textScale)
            text(xpoint[xLen],ypoint[xLen],labels=rev(labelsPerc)[xLen],adj=c(1,1),cex=textScale)

            text(xpoint[-c(1,xLen)],ypoint[-c(1,xLen)],labels=rev(labelsText)[-c(1,xLen)],adj=c(0,1),cex=textScale)
            text(xpoint[-c(1,xLen)],ypoint[-c(1,xLen)],labels=rev(labelsPerc)[-c(1,xLen)],adj=c(1,1),cex=textScale)
            text(xpoint[1],title,lab='Matching SNPs',font=2,adj=c(1,.5),cex=textScale)
            text(xpoint[1],title,lab='  Relatedness',font=2,adj=c(0,.5),cex=textScale)
#Original
            # map <- makecmap(0:500,420,colFn=colorFunction2)
            # a <- distogram(clust,map,key=F)
            par <- opar

            },silent=T)


        # title('Sample identity distogram',line=-2.3,cex.main=textScale)
        title('Sample identity distogram',line=-1.5,cex.main=textScale)
        mtext(paste(format(Sys.time(), "%F %H:%M:%S")),3,padj=+3,adj=0.99,cex=textScale)
        # box()

        dev.off()
    }

    # browser()
    ##The copy number clustering:  <----------------- Den är på

    if(length(tableList) > 2) {
    #browser()
        #shortsegNames <- sapply(shortsegList,function(x) {names(x)[1]})
        shortsegNames <- colnames(profiles)
        profiles <- do.call('cbind',shortsegList)
        colnames(profiles) <- shortsegNames
        rownames(profiles) <- NULL
        colnames(profiles) <- strtrim(colnames(profiles),70)
        preClust <- dist(t(profiles))
        h <- hclust(preClust)
        order <- h$order

        ### hit är det moddat.

        clust <- dist(t(profiles[,order]))
        a <- hclust(clust)
        names <- a$labels

        tableListOri <- tableList

        #Use segmentList instead of tableList   <------------------   Stängde av det
        data(chromLengths,package='rawcopy')
        if(F & exists('segmentList')){
            for(n in 1:length(segmentList)) { colnames(segmentList[[n]]) <- c("chr", "start", "end", "avlog", "imba")}
            tableList <- segmentList
        }


        #Fix chromosome starts
        for(i in 1:length(tableList)) {
            data(chromLengths,package='rawcopy')
            tableList[[i]][1:10,]
            tableList[[i]]$startbefore <- tableList[[i]]$start
            tableList[[i]]$endbefore <- tableList[[i]]$end
            for(chr in unique(chromLengths$chr)) {
                tableList[[i]]$startbefore[tableList[[i]]$chr == chr] <-
                    tableList[[i]]$start[tableList[[i]]$chr == chr] + chromLengths$before[chromLengths$chr == chr]
                tableList[[i]]$endbefore[tableList[[i]]$chr == chr] <-
                    tableList[[i]]$end[tableList[[i]]$chr == chr] + chromLengths$before[chromLengths$chr == chr]
            }
        }

        # png('~/tessst2.png',width=20,height=20*.7070,units='in',res=300)
        # png('~/tessst2.png',width=20,height=10,units='in',res=300)
        # png('~/tessst2.png',width=20,height=10+length(tableList) * 0.1,units='in',res=300)
        png(file=paste(dir,'/sampleClust_',format(Sys.time(), "%F-%H_%M_%S"),'.png',sep=''),width=20,height=6+length(tableList) * 0.1,units='in',res=300)
        tr <- 0.10
        tb <- tr*2
        # tr <- 0.2
        # tb <- .3
        bot1 <- 0.01
        bot <- 0.0
        top1 <- 0.96
        top <- 1
        left <- 0.01
        right <- 0.99
        shareline <- 0.2
        if(length(tableList) > 50) shareline <- 0.075
        if(length(tableList) < 10) shareline <- 0.4
        close.screen(all=T)
        split.screen(as.matrix(data.frame(
                                          left=c(0,0),
                                          right=c(1,1),
                                          bottom=c(shareline,bot1),
                                          top=c(top1,shareline))))
        screen(1)
        split.screen(as.matrix(data.frame(
                                          left=c(left,tr,tb),
                                          right=c(tr,tb,right),
                                          bottom=c(bot,bot,bot),
                                          top=c(top,top,top))))
        screen(2)
        split.screen(as.matrix(data.frame(
                                          left=c(left,tb),
                                          right=c(tb,right),
                                          bottom=c(bot,bot),
                                          top=c(top,top))))
        #Screen layout
        #    _____
        #   |3 4 5|
        #   |6   7|
        #   -------
        indx <- T
        i <- 1
        l <- 1:length(tableList)

        #plot dendrogram
        screen(3)
        par(mar=c(0,0,0,0),xaxs='i',yaxs='i',lend=2)
        plot(as.phylo(a),label.offset = 0,font=1,show.tip.label=FALSE,edge.width=2,y.lim=c(0,length(l)+1),no.margin=T)
        # plot(as.phylo(a),y.lim=c(0,length(l)+1))


        #plot names
        screen(4)
        par(mar=c(0,0,0,0),xaxs='i',yaxs='i')
        plot(1,ylim=c(0,length(l)+1),xlim=c(.9,1.1),type='n',ylab='',xlab='',axes=F)
        text(x=.894,y=(1:(length(l)+0)),labels=names,pos=4,cex=0.5)
        segments(x0=0,x1=10,y0=c(.5+(0:(length(order)+1))),y1=c(.5+(0:(length(order)+1))),lwd=1,col='lightgrey')

        #Cluster plot
        screen(5)
        par(mar=c(0,0,0,0),xaxs='i',yaxs='i')
        plot(tableList[[order[1]]]$startbefore[indx],rep(1,length(tableList[[order[1]]]$startbefore))[indx],ylim=c(1,length(l)+2),pch=15,col='#00000020',type='n',axes=F)
        counter <- 1
        for(i in order) {
            #Create colormatrix
            x <- 1.2
            xtop <- seq(-x,x,0.01)
            xtopcol <- colorRampPalette(c("blue", "lightgrey",'green'))(length(xtop))

            xbot <- seq(-x,x,0.01)
            xbotcol <- colorRampPalette(c("blue", "black",'green'))(length(xbot))

            matrixcol <- NULL
            y <- 100
            yres <- 1 / (1/y)
            for(k in 1:length(xtop)) {
                topcol <- xtopcol[k]
                botcol <- xbotcol[k]
                columncol <- colorRampPalette(c(topcol,botcol))(yres)
                matrixcol <- cbind(matrixcol,columncol)
            }
            matrixcol <- matrixcol[nrow(matrixcol):1,]
            colnames(matrixcol) <- round(seq(-x,x,0.01),2)
            rownames(matrixcol) <- seq(0,1,length.out=y)

            #Custom color for each segment
            col <- NULL
            for(j in 1:nrow(tableList[[i]])) {
                avlog <- round(tableList[[i]]$avlog[j],2)
                imba <- round(tableList[[i]]$imba[j],1)
                if(is.na(imba)) {
                    if(tableList[[i]]$chr[j] == 'chrY') {
                        imba <- 0.9
                    } else {
                        imba <- 0.1
                    }
                }
                if(is.na(avlog)) avlog <- 0
                # col <- c(col,'cyan')
                # next()

                if(avlog < -x) avlog <- -x; if(avlog > x) avlog <- x
                if(imba < 0) imba <- 0; if(imba > 1) imba <- 1
                curCol <- matrixcol[which.min(abs(as.numeric(rownames(matrixcol)) - imba)),as.character(avlog)]
                col <- c(col,curCol)
            }


            rect(xleft=tableList[[i]]$startbefore[indx],xright=tableList[[i]]$endbefore[indx],ytop=1.5+counter,ybottom=.5+counter,col=col[indx],border=NA)
            counter <- counter+1
        }

        chroms <- chromLengths$length + chromLengths$before
        starts <- tapply(tableList[[1]]$startbefore,tableList[[1]]$chr,max)
        stops  <- tapply(tableList[[1]]$endbefore,tableList[[1]]$chr,max)
        x0 <- c(stops[1],starts[c(-1,-which(names(starts) == 'chrM'))])
        #horizontal line
        segments(x0=0,x1=4e9,y0=c(.5+(0:length(order))),y1=c(.5+(0:length(order))),lwd=1,col='white')

        #vertical lines
        # segments(x0=chroms,x1=chroms,y0=-1,y1=1e4,col='black',lwd=1,lty=1)
        segments(x0=chroms,x1=chroms,y0=1.5,y1=counter+.5,col='black',lwd=2,lty=1,lend=1)

        #add chromosomes
        chromname <- sub('chr','',chromLengths$Chromosome)
        chromname[chromname == 'Y'] <- ' Y'
        chromname[chromname == 'X'] <- ' X'
        # text((chromLengths$length / 2 + chromLengths$before)*0.992,(2+length(l))*1.007,labels=chromname,cex=1.1,adj=c(0,1.6),col='black')
        # text((chromLengths$length / 2 + chromLengths$before)*0.992,1.66,labels=chromname,cex=1.1,adj=c(0,1.6),col='black')

        #Colorplot
        screen(6)
        par(mar=c(1,2,1,3),xaxs='r',yaxs='r')
        plot(rep(1:length(xtop),yres),rep(1:yres,each=length(xtop)),col=as.vector(t(matrixcol)),pch=15,cex=5,axes=F,xlab='',ylab='')

        #Fix axis
        # whole=c(-0.6,0,0.37,0.63)
        x <- NULL
        for(h in c(-0.6,0,0.37,0.63)) {
            x <- c(x,which.min(abs(as.numeric(colnames(matrixcol)) - h)))
        }

        y <- NULL
        for(h in c(0.08,0.33,0.5)) {
            y <- c(y,which.min(abs(as.numeric(rownames(matrixcol)) - h)))
        }
        axis(side=1,cex.axis=0.6,tck=-0.03,at=x,las=1,labels=c('-50%','100%','+50%','+100%'),col='black',col.ticks='black',lend=3,padj=-2,lwd=1)
        axis(side=2,cex.axis=0.6,tck=-0.03,at=y,labels=c('1:1','1:2','1:3'),las=1,col='black',col.ticks='black',lend=1)
        box(lwd=1.5)


        #pileup
        screen(7)
        rbinded <- do.call(rbind,tableList)
        par(mar=c(1,0,1,0),xaxs='i',yaxs='i')
        plot(tableList[[order[1]]]$startbefore[indx],rep(1,length(tableList[[order[1]]]$startbefore))[indx],ylim=c(-1,1),type='n',axes=F,xlab='',ylab='')
        segments(x0=chroms,x1=chroms,y0=-1,y1=1,col='grey',lty=2)
        segments(x0=4e9,x1=0,y0=seq(-1,1,0.5),y1=seq(-1,1,0.5),col='grey',lty=2)
        axis(2,las=1,tck=-0.02,hadj=0.55,cex.axis=0.6)
        # axis(1,at=(chromLengths$length / 2 + chromLengths$before)*0.993,labels=chromname,las=1,tick=F,tck=-0.02,padj=-2,cex.axis=0.7)
        axis(1,at=(chromLengths$length / 2 + chromLengths$before)*1,labels=chromname,las=1,tick=F,tck=-0.02,padj=-2,cex.axis=0.6)
        for(val in c(0.15)) {
            if(val == 0.15) color <- c('darkgreen','blue')
            if(val == 0.5) color <- c('orange','blue')
            gain <- rbinded$avlog > val
            loss <- rbinded$avlog < -val

            pileGain <- pileup(rbinded[gain,])
            pileLoss <- pileup(rbinded[loss,])

            pileGain$rel <- pileGain$Count/length(tableList)
            pileLoss$rel <- pileLoss$Count/length(tableList)

            #fix before
            pileGain$startbefore <- pileGain$Start
            pileGain$endbefore <- pileGain$End
            pileLoss$startbefore <- pileLoss$Start
            pileLoss$endbefore <- pileLoss$End
            for(chr in unique(chromLengths$chr)) {
                pileGain$startbefore[pileGain$Chromosome == chr] <-
                    pileGain$Start[pileGain$Chromosome == chr] + chromLengths$before[chromLengths$chr == chr]
                pileGain$endbefore[pileGain$Chromosome == chr] <-
                    pileGain$End[pileGain$Chromosome == chr] + chromLengths$before[chromLengths$chr == chr]

                pileLoss$startbefore[pileLoss$Chromosome == chr] <-
                    pileLoss$Start[pileLoss$Chromosome == chr] + chromLengths$before[chromLengths$chr == chr]
                pileLoss$endbefore[pileLoss$Chromosome == chr] <-
                    pileLoss$End[pileLoss$Chromosome == chr] + chromLengths$before[chromLengths$chr == chr]
            }

            xleft   <- pileGain$startbefore
            ybottom <- 0
            xright  <- pileGain$endbefore
            ytop    <- pileGain$rel
            rect(xleft=xleft,ybottom=ybottom,xright=xright,ytop=ytop,col=color[1],border=NA)
            xleft   <- pileLoss$startbefore
            xright  <- pileLoss$endbefore
            ytop    <- pileLoss$rel
            rect(xleft=xleft,ytop=ybottom,xright=xright,ybottom=-ytop,col=color[2],border=NA)
        }
        close.screen(all=T)
        par(new=T)
        plot(1,type='n',axes=F,xlab='',ylab='')
        mtext('Clusters',3,padj=-2.75,cex=1.25)
        mtext(paste(format(Sys.time(), "%F %H:%M:%S")),3,padj=-2.9,adj=1,cex=1)
        dev.off()

    }

    # vkey(a,n=20)
    # dev.off()
    # return(clust)
    return(NULL)
}

#                                                 __  __       _
# _ __ __ ___      _____ ___  _ __  _   _   __ _ / _|/ _|_   _/ |
#| '__/ _` \ \ /\ / / __/ _ \| '_ \| | | | / _` | |_| |_| | | | |
#| | | (_| |\ V  V / (_| (_) | |_) | |_| || (_| |  _|  _| |_| | |
#|_|  \__,_| \_/\_/ \___\___/| .__/ \__, (_)__,_|_| |_|  \__, |_|
#                            |_|    |___/                |___/
#rawcopy.affy1

# (shared functions for both snp6 and cytoscan)
rawcopy.affy1 <- function(name, celfile,
                          referenceFile,
                          wavinessFile,
                          outdir,
                          cdf,
                          annot, pannot,
                          snpClusters, probeClusters,
                          writeData=TRUE,
                          allSNPs=FALSE,
                          MDS=TRUE
) {

    data('chromData',package='rawcopy')
    data('chromLengths',package='rawcopy')
    data('refseq.genes',package='rawcopy')

    cat(format(Sys.time(), "%F %H:%M:%S"),'Parsing:',name,'\n')
    intensities <- matrix(celfile$intensities, nrow=celfile$header$cols, ncol=celfile$header$rows)
    ##### add extaction of array type from cel here #
    array=celfile$header$chiptype
    rm(celfile)
    probeNames <- names(cdf)
    if (array=='GenomeWideSNP_6') {
        probeIx <- grep('CN',as.character(probeNames))
        snpIx <- grep('SNP',as.character(probeNames))
    }
    if (array=='CytoScanHD_Array' | array=='CytoScan750K_Array') {
        probeIx <- grep('C-',as.character(probeNames))
        snpIx <- grep('S-',as.character(probeNames))
    }

    pannot <- pannot[match(as.character(probeNames[probeIx]), as.character(pannot$Probe.Set.ID)),]
    annot <- annot[match(as.character(probeNames[snpIx]), as.character(annot$Probe.Set.ID)),]

    # Replace '---' with NA in annot & pannot.
    # annot$Physical.Position[which(as.character(annot$Physical.Position) == '---')] <- NA
    # pannot$Chromosome.Start[which(as.character(pannot$Chromosome.Start) == '---')] <- NA

    suppressWarnings(snporder <- order(annot$Chromosome, as.numeric(as.character(annot$Physical.Position))))
    suppressWarnings(probeorder <- order(pannot$Chromosome, as.numeric(as.character(pannot$Chromosome.Start))))

    annot <- annot[snporder,]
    snpIx <- snpIx[snporder]
    pannot <- pannot[probeorder,]
    probeIx <- probeIx[probeorder]

    snpClusters <- snpClusters[match(annot$Probe.Set.ID, as.character(snpClusters$Name)),]
    probeClusters <- probeClusters[match(pannot$Probe.Set.ID, as.character(probeClusters$Name)),]


    # The Y fix
    # ix=pannot$Chromosome=='Y'
    # probeClusters$k[ix]=probeClusters$haploid.k[ix]#+median(probeClusters$k,na.rm=T)-median(probeClusters$haploid.k,na.rm=T)
    # probeClusters$m[ix]=probeClusters$haploid.m[ix]#+median(probeClusters$m,na.rm=T)-median(probeClusters$haploid.m,na.rm=T)
    # ix=annot$Chromosome=='Y'
    # snpClusters$k[ix]=snpClusters$haploid.k[ix]#+median(snpClusters$k,na.rm=T)-median(snpClusters$haploid.k,na.rm=T)
    # snpClusters$m[ix]=snpClusters$haploid.m[ix]#+median(snpClusters$m,na.rm=T)-median(snpClusters$haploid.m,na.rm=T)

    #browser()

    # ____  _       _   _
    #|  _ \| | __ _| |_| |_ _ __   ___  _ __ _ __ ___
    #| |_) | |/ _` | __| __| '_ \ / _ \| '__| '_ ` _ \
    #|  __/| | (_| | |_| |_| | | | (_) | |  | | | | | |
    #|_|   |_|\__,_|\__|\__|_| |_|\___/|_|  |_| |_| |_|
    #
    #Plattnorm

    ## For chip regional hybridization levels
    m <- matrix(NA,nrow=nrow(intensities),ncol=ncol(intensities))
    snp_nsp <- snp_sty <- snp_both <- probe_nsp <- probe_sty <- probe_both <- m
    nsp <- !annot$sty; sty <- !annot$nsp
    for (i in 1:length(snpIx)) {
        ix1 <- cdf[[snpIx[i]]][[1]][[1]][[1]]
        ix2 <- cdf[[snpIx[i]]][[1]][[2]][[1]]
        if (nsp[i]) {
            snp_nsp[ix1] <- (intensities[ix1])
            snp_nsp[ix2] <- (intensities[ix2])
        } else if (sty[i]) {
            snp_sty[ix1] <- (intensities[ix1])
            snp_sty[ix2] <- (intensities[ix2])
        } else {
            snp_both[ix1] <- (intensities[ix1])
            snp_both[ix2] <- (intensities[ix2])
        }
    }
    nsp <- !pannot$sty; sty <- !pannot$nsp
    for (i in 1:length(probeIx)) {
        ix <- cdf[[probeIx[i]]][[1]][[1]][[1]]
        if (nsp[i]) {
            probe_nsp[ix] <- intensities[ix]
        } else if (sty[i]) {
            probe_sty[ix] <- intensities[ix]
        } else {
            probe_both[ix] <- intensities[ix]
        }
    }
    # smooth snp_avg and probe_avg
    m.snp_nsp <- snp_nsp; snp_nsp[is.nan(snp_nsp)] <- NA
    m.snp_sty <- snp_sty; snp_sty[is.nan(snp_sty)] <- NA
    m.snp_both <- snp_both; snp_both[is.nan(snp_both)] <- NA
    m.probe_nsp <- probe_nsp; probe_nsp[is.nan(probe_nsp)] <- NA
    m.probe_sty <- probe_sty; probe_sty[is.nan(probe_sty)] <- NA
    m.probe_both <- probe_both; probe_both[is.nan(probe_both)] <- NA
    left <- seq(1,ncol(intensities)-9,10)
    right <- seq(10,ncol(intensities),10); right[length(right)] <- ncol(intensities)
    top <- seq(1,nrow(intensities)-9,10)
    bottom <- seq(10,nrow(intensities),10); bottom[length(bottom)] <- nrow(intensities)
    for (i in 1:length(top)) { # rad för rad
        ixrow <- max(1,top[i]-30):min(bottom[i]+30,nrow(intensities))
        #print(i)
        for (j in 1:length(left)) { # kolumnvis
            ixcol <- max(1,left[j]-30):min(right[j]+30,ncol(intensities))
            m.snp_nsp[top[i]:bottom[i],left[j]:right[j]] <- median(snp_nsp[ixrow,ixcol],na.rm=T)
            m.snp_sty[top[i]:bottom[i],left[j]:right[j]] <- median(snp_sty[ixrow,ixcol],na.rm=T)
            m.snp_both[top[i]:bottom[i],left[j]:right[j]] <- median(snp_both[ixrow,ixcol],na.rm=T)
            m.probe_nsp[top[i]:bottom[i],left[j]:right[j]] <- median(probe_nsp[ixrow,ixcol],na.rm=T)
            m.probe_sty[top[i]:bottom[i],left[j]:right[j]] <- median(probe_sty[ixrow,ixcol],na.rm=T)
            m.probe_both[top[i]:bottom[i],left[j]:right[j]] <- median(probe_both[ixrow,ixcol],na.rm=T)
        }
    }


    ### Read SNPS
    nsp <- !annot$sty; sty <- !annot$nsp
    baf <- a <- b <- lrr <- lr <- snpHyb <- rep(NA, length(snpIx))
    for (i in 1:length(snpIx)) {
        ix1 <- cdf[[snpIx[i]]][[1]][[1]][[1]]
        ix2 <- cdf[[snpIx[i]]][[1]][[2]][[1]]
        ta <- (intensities[ix1])
        tb <- (intensities[ix2])
        if (nsp[i]) {
            hybA <- m.snp_nsp[ix1]
            hybB <- m.snp_nsp[ix2]
        } else if (sty[i]) {
            hybA <- m.snp_sty[ix1]
            hybB <- m.snp_sty[ix2]
        } else {
            hybA <- m.snp_both[ix1]
            hybB <- m.snp_both[ix2]
        }
        o=outliers(ta,tb,0.3) ### Kolla om den här duger
        #print(sum(o))
        ####################### Ändra också så att "a" och "b" väljs med intensitet inom 9.5-11 i stället.
        ta <- mean(ta[!o])
        tb <- mean(tb[!o])
        hybA <- mean(hybA[!o])
        hybB <- mean(hybB[!o])
        lr[i] <- log2(sqrt(ta^2+tb^2))
        snpHyb[i] <- mean(log2(c(hybA,hybB)))
        a[i] <- ta
        b[i] <- tb
    }
    raf <- b/(a+b)


    ### Read probes
    nsp <- !pannot$sty; sty <- !pannot$nsp
    lir <- li <- probeHyb <- rep(NA, length(probeIx))
    for (i in 1:length(lir)) {
        ix <- cdf[[probeIx[i]]][[1]][[1]][[1]]
        li[i] <- mean(log2(intensities[ix])) # nearly always just one probe though.
        if (nsp[i]) {
            probeHyb[i] <- mean(log2(m.probe_nsp[ix]))
        } else if (sty[i]) {
            probeHyb[i] <- mean(log2(m.probe_sty[ix]))
        } else {
            probeHyb[i] <- mean(log2(m.probe_both[ix]))
        }
    }

    ## RAW SNPs and probes are now stored as raf+lr and li.

    #browser()


    ## Produce QC.
    QC=NULL
    cat(format(Sys.time(), "%F %H:%M:%S"),'Quality control:',name,'\n')
    ## SNPs:
    QC$snp.nsp=round(median(lr[!annot$sty],na.rm=T),2)
    QC$snp.sty=round(median(lr[annot$sty & !annot$nsp],na.rm=T),2)
    QC$snp.both=round(median(lr[annot$nsp & annot$sty], na.rm=T),2)
    QC$snp.MAPD=round(median(abs(diff(lr)), na.rm=T),2)
    Aut=(annot$Chromosome!='X' & annot$Chromosome!='Y' & annot$Chromosome!='MT')
    QC$snp.X=round(median(lr[annot$Chromosome=='X'],na.rm=T) -
                       median(lr[Aut],na.rm=T),2)
    QC$snp.Y=round(median(lr[annot$Chromosome=='Y'],na.rm=T) -
                       median(lr[Aut],na.rm=T),2)
    QC$snp.MT=round(median(lr[annot$Chromosome=='MT'],na.rm=T) -
                        median(lr[Aut],na.rm=T),2)
    ## Probes:
    QC$probe.nsp=round(median(li[!pannot$sty],na.rm=T),2)
    QC$probe.sty=round(median(li[pannot$sty & !pannot$nsp],na.rm=T),2)
    QC$probe.both=round(median(li[pannot$nsp & pannot$sty], na.rm=T),2)
    QC$probe.MAPD=round(median(abs(diff(li)), na.rm=T),2)
    Aut=(pannot$Chromosome!='X' & pannot$Chromosome!='Y' & pannot$Chromosome!='MT')
    QC$probe.X=round(median(li[pannot$Chromosome=='X'],na.rm=T) -
                         median(li[Aut],na.rm=T),2)
    QC$probe.Y=round(median(li[pannot$Chromosome=='Y'],na.rm=T) -
                         median(li[Aut],na.rm=T),2)
    QC=as.data.frame(QC)

    #browser()


    #load('~/Dropbox/CytoScan.snpClusters.2014-11-28_11_57_48.Rdata')
    #load('~/Dropbox/SNP6.snpClusters.2014-12-05_14_23_38.Rdata')
    #snpClusters <- snpClusters[match(annot$Probe.Set.ID, as.character(snpClusters$Name)),]
    # remove this if meanVal not already andjusted to peak:
    #if (length(snpClusters$peak)>0) snpClusters$meanVal=snpClusters$meanVal-snpClusters$peak
    #browser()

    ## Compute BAF (SNPs)
    cat(format(Sys.time(), "%F %H:%M:%S"),'BAF estimation:',name,'\n')
    bestdelta=0
    m=Inf; ix=seq(1,length(raf),100)
    for (delta in seq(-1,1,0.01)) suppressWarnings(try( {
        baf=getBAF(raf[ix],lr[ix]+delta,snpClusters[ix,],allSNPs=F)
        t=lr[ix][!is.na(baf)]
        baf=baf[!is.na(baf)]
        baf=abs(baf-0.5)
        t=t[baf>0.4]
        baf=baf[baf>0.4]
        l=line(t,baf)
        l=abs(l$coefficients[2])
        if (l<m) {
            m=l
            bestdelta=delta
        }
    }, silent=T))
    # browser()
    baf=getBAF(raf,lr+bestdelta,snpClusters,allSNPs)


    cat(format(Sys.time(), "%F %H:%M:%S"),'SNP normalization:',name,'\n')
    #browser()

    ## Compute raw Log-R-Ratio (SNPs)
    hyb=rep(QC$snp.both,length(lr))
    hyb[!annot$nsp]=QC$snp.sty # relevant with SNP6 only
    hyb[!annot$sty]=QC$snp.nsp

    # Line based alternative:
    ## Find optimal "hyb"
    #     bestdelta=0
    #     m=Inf; ix=seq(1,length(lr),3)
    #     for (delta in seq(-1,+1,0.01)) {
    #         temp <- lr[ix] - (snpClusters$k[ix]*(hyb[ix]+delta)+snpClusters$m[ix])
    #         temp <- median(abs(diff(temp)),na.rm=T)
    #         if (temp<m) {
    #             m=temp
    #             bestdelta=delta
    #         }
    #     }
    #      lrr <- lr - (snpClusters$k*(hyb+bestdelta)+snpClusters$m)
    #          median(abs(diff(lrr)), na.rm=T)

    hyb=snpHyb # <---------------------------------  Återaktivera denna och en till (nedan) för plattnormalisering

    ## Find optimal "delta" and "mapd"
    bestdelta=0; bestMAPD=QC$snp.MAPD
    m=Inf; t=NA; ix=seq(1,length(lr),10)
    for (delta in seq(-1,1,0.05)) {
        for (mapd in seq(bestMAPD-0.1,bestMAPD+0.1,0.02)) {
            temp <- lr[ix] - (snpClusters$k[ix]*(hyb[ix]+delta)+snpClusters$k2[ix]*mapd+snpClusters$m[ix])
            temp <- median(abs(diff(temp)),na.rm=T)
            t=c(t,temp)
            if (temp<m) {
                m=temp
                bestdelta=delta
                bestMAPD=mapd
            }
        }
    }
    lrr <- lr - (snpClusters$k*(hyb+bestdelta)+snpClusters$k2*bestMAPD+snpClusters$m)
    median(abs(diff(lrr)), na.rm=T)

    ## Do the genotype normalization
    # normalize lrr for BAF-related bias
    offset=rep(0,length(lrr))
    ix=snpClusters$good & baf>0.5; ix[is.na(ix)]=F
    bb_comp=2*(baf[ix]-0.5); bb_comp[bb_comp>1]=1
    ab_comp=1-bb_comp
    offset[ix]=snpClusters$ab.median[ix]*ab_comp + snpClusters$bb.median[ix]*bb_comp - snpClusters$meanVal[ix]
    ix=snpClusters$good & baf<=0.5; ix[is.na(ix)]=F
    ab_comp=2*baf[ix]; ab_comp[ab_comp<0]=0
    aa_comp=1-ab_comp
    offset[ix]=snpClusters$aa.median[ix]*aa_comp + snpClusters$ab.median[ix]*ab_comp - snpClusters$meanVal[ix]
    lrr=lrr-offset
    median(abs(diff(lrr)),na.rm=T)

    lrr[annot$Chromosome=='---']=NA
    ## Normalize Log-R-Ratio (SNPs)
    ## norm for mult factors
    if (array=='GenomeWideSNP_6')
        lrr <- norm3(data = lrr,
                     factor1 = annot$nsp.length,
                     factor2 = annot$sty.length,
                     factor3 = annot$gc,
                     window = 0.05)
    if (array=='CytoScanHD_Array' | array=='CytoScan750K_Array')
        lrr <- norm2(data = lrr,
                     factor1 = annot$nsp.length,
                     factor2 = annot$nsp.gc,
                     window = 0.02)

    round(median(abs(diff(lrr)), na.rm=T),3)

    ## Adjust BAF to remove residual lrr-related bias
    #browser()
    ix=seq(1,length(lrr),100)
    #plot(lrr[ix],baf[ix])
    tbaf=abs(baf-0.5)
    tlrr=lrr[tbaf>0.4]
    tbaf=2*tbaf[tbaf>0.4]
    l=line(tlrr,tbaf)
    coeff=l[[2]][1]+l[[2]][2]*lrr
    baf=0.5+(baf-0.5)/coeff
    ### standardise BAF range
    d=density(abs(baf-0.5)[abs(baf-0.5)>0.4],na.rm=T)
    scale=0.5/d$x[which.max(d$y)]
    baf=0.5+scale*(baf-0.5)


    ## Store as snps.txt
    ix=annot$Chromosome!='---'
    snps.txt <- data.frame(Probe.Set.ID=annot$Probe.Set.ID[ix],   stringsAsFactors=F,
                           Chromosome=chrom_ucsc(annot$Chromosome[ix]),
                           Start=as.integer(as.character(annot$Physical.Position[ix])),
                           End=as.integer(as.character(annot$Physical.Position[ix])),
                           Value=round(baf[ix],4),
                           Quality=as.numeric(snpClusters$good[ix]),
                           Raw.A=round(a[ix]),
                           Raw.B=round(b[ix]),
                           Allele.A=annot$Allele.A[ix],
                           Allele.B=annot$Allele.B[ix]
    )
    snps.txt=snps.txt[order(chrom_n(snps.txt$Chromosome),snps.txt$Start),]

    cat(format(Sys.time(), "%F %H:%M:%S"),'Probe normalization:',name,'\n')
    #browser()
    ## Compute raw Log-Intensity-Ratio (probes)
    hyb=rep(QC$probe.both,length(li))
    hyb[!pannot$nsp]=QC$probe.sty
    hyb[!pannot$sty]=QC$probe.nsp
    #load('~/Dropbox/CytoScanHD.probeClusters.2014-11-28_10_06_24.Rdata')
    #load('~/Dropbox/SNP6.probeClusters.2014-12-05_11_43_13.Rdata')
    #probeClusters <- probeClusters[match(pannot$Probe.Set.ID, as.character(probeClusters$Name)),]
    # remove if not adj to peak already:
    #if (length(probeClusters$peak)>0) probeClusters$meanVal=probeClusters$meanVal-probeClusters$peak

    # Line based alternative
    ## Find optimal hyb and use
    #     bestdelta=0
    #     m=Inf; ix=seq(1,length(li),3)
    #     for (delta in seq(-1,1,0.05)) {
    #         temp <- li[ix] - (probeClusters$k[ix]*(hyb[ix]+delta)+probeClusters$m[ix])
    #         temp <- median(abs(diff(temp)),na.rm=T)
    #         if (temp<m) {
    #             m=temp
    #             bestdelta=delta
    #         }
    #     }
    #     lir <- li - (probeClusters$k*(hyb+bestdelta)+probeClusters$m)
    #     median(abs(diff(lir)),na.rm=T)


    hyb=probeHyb # <----------------------------------------  Återaktivera denna och en till (ovan) för plattnormalisering

    ## Find optimal hyb and MAPD to use (add hyb-STY???)
    bestdelta=0; bestMAPD=QC$probe.MAPD
    m=Inf; ix=seq(1,length(li),5)
    for (delta in seq(-1,1,0.05)) {
        for (mapd in seq(bestMAPD-0.1,bestMAPD+0.1,0.02)) {
            temp <- li[ix] - (probeClusters$k[ix]*(hyb[ix]+delta)+probeClusters$k2[ix]*mapd+probeClusters$m[ix])
            temp <- median(abs(diff(temp)),na.rm=T)
            if (temp<m) {
                m=temp
                bestdelta=delta
                bestMAPD=mapd
            }
        }
    }
    lir <- li - (probeClusters$k*(hyb+bestdelta)+probeClusters$k2*bestMAPD+probeClusters$m)
    median(abs(diff(lir)),na.rm=T)

    # __  __
    #|  \/  | __ _ _ __  _   _ ___
    #| |\/| |/ _` | '_ \| | | / __|
    #| |  | | (_| | | | | |_| \__ \
    #|_|  |_|\__,_|_| |_|\__,_|___/
    #
    #Manus
    manus <- 0
    if(manus == 1) {
        # par(mfrow=2:1)
        # plot(pannot$nsp.length[indx],lir[indx],col='#00000010',pch=16,xlim=c(200,800),ylim=c(-2.5,.5))
        # plot(pannot$nsp.length[indx],li[indx],col='#00000010',pch=16,xlim=c(200,800),ylim=c(6,12))

        pdf(paste('/media/safe/bjorn/2014/rawcopyImages/fraglength_lir_GSM1317177_',
                  format(Sys.time(), "%F-%H_%M_%S"),'.pdf',sep=''),width=5,height=5)

        indx <- seq(1,length(lir),by=50)
        par(mar=c(3.5,3.5,3.5,3.5),xaxs='i',yaxs='i',lend=1,las=1)
        plot(pannot$nsp.length[indx],lir[indx],type='n',
             col='#00000010',pch=16,axes=F,
             main='GSM1317177_H050154T-CytoScanHD_522-H050154T',
             ylab='',xlab='',xlim=c(200,800),ylim=c(-2.5,.5))
        axis(1,lwd=2,cex.axis=1.5)
        axis(2,lwd=2,cex.axis=1.5)
        grid(col='#00000040',lwd=1.5,lty=2)
        box(lwd=2)
        points(pannot$nsp.length[indx],lir[indx],col='#00000010',pch=16)
        dev.off()
    }

    #browser()

    lir[pannot$Chromosome=='---']=NA
    # The mult-factor norm:
    if (array=='GenomeWideSNP_6') lir <- norm3(data = lir,
                                               factor1 = pannot$nsp.length,
                                               factor2 = pannot$sty.length,
                                               factor3 = pannot$gc,
                                               window = 0.05)
    if (array=='CytoScanHD_Array' | array=='CytoScan750K_Array') lir <- norm2(data = lir,
                                                                              factor1 = pannot$nsp.length,
                                                                              factor2 = pannot$nsp.gc,
                                                                              window = 0.02)
    median(abs(diff(lir)),na.rm=T)

    # browser()


    ## Store as probes.txt
    ix1=annot$Physical.Position != '---'
    ix2=pannot$Chromosome.Start!= '---'
    probes.txt <- data.frame(Probe.Set.ID=c(annot$Probe.Set.ID[ix1],pannot$Probe.Set.ID[ix2]),   stringsAsFactors=F,
                             #Chromosome.annot=c(annot$Chromosome,pannot$Chromosome),
                             Chromosome=chrom_ucsc(c(annot$Chromosome[ix1],pannot$Chromosome[ix2])),
                             Start=as.integer(c(as.character(annot$Physical.Position[ix1]),as.character(pannot$Chromosome.Start[ix2]))),
                             End=as.integer(c(as.character(annot$Physical.Position[ix1]),as.character(pannot$Chromosome.Stop[ix2]))),
                             Value=round(c(lrr[ix1],lir[ix2]),8),
                             Raw.Intensity=round(c(lr[ix1],li[ix2]),4),
                             B.Allele.Frequency=round(c(baf[ix1],rep(NA,sum(ix2))),4)
    )
    probes.txt=probes.txt[order(chrom_n(probes.txt$Chromosome),probes.txt$Start),]
    #Marker.SD=c(abs(snpClusters$meanResidual[ix1]),abs(probeClusters$meanResidual[ix2]))
    median(abs(diff(probes.txt$Value)),na.rm=T)

    #Match lir and lrr levels
    m=Inf
    ix=grep('CN',probes.txt$Probe.Set.ID)
    val=probes.txt$Value
    for (shift in seq(-0.1,0.1,0.01)) {
        tval=val
        tval[ix]=tval[ix]+shift
        temp=median(abs(diff(tval)),na.rm=T)
        if (temp<m) {
            m=temp
            bestshift=shift
        }
    }
    probes.txt$Value[ix]=probes.txt$Value[ix]+bestshift
    median(abs(diff(probes.txt$Value)),na.rm=T)

    #browser()
    # Regional GC content normalization on probes.txt
    data('gc100k',package='rawcopy')
    gc=gc100k
    #load('~/Dropbox/GC_hg19/gc100k-3M-snp6.Rdata')
    #gc=gc_snp6[c(1,7)]
    gc=as.numeric(gc[match(probes.txt$Probe.Set.ID, gc[,1]),2])

    l=line(gc,probes.txt$Value)
    probes.txt$Value=probes.txt$Value - (l$coefficients[2]*gc+l$coefficients[1])


    # __  __ ____  ____
    #|  \/  |  _ \/ ___|
    #| |\/| | | | \___ \
    #| |  | | |_| |___) |
    #|_|  |_|____/|____/
    #
    #MDS
    # if (array=='CytoScanHD_Array' | array=='CytoScan750K_Array' | array == 'GenomeWideSNP_6') {
    if ((array=='CytoScanHD_Array' |  array == 'GenomeWideSNP_6') & MDS==T) {
        # oldMAPD <- median(abs(diff(probes.txt$Value)),na.rm=T)
        # load('/media/safe/bjorn/git/rawcopy/rawcopyReference/data/Cytoscan-CEL/symbolicSummary/ref6_2015-03-24.Rdata')
        seqA <- c(-40,40)
        seqB <- c(-25,20)
        seqC <- c(-20,20)
        seqD <- c(-15,20)
        seqE <- c(-20,20)
        seqF <- c(-15,15)
        A <- seq(seqA[1],seqA[2],by=diff(seqA)/40)
        B <- seq(seqB[1],seqB[2],by=diff(seqB)/40)
        C <- seq(seqC[1],seqC[2],by=diff(seqC)/40)
        D <- seq(seqD[1],seqD[2],by=diff(seqD)/40)
        E <- seq(seqE[1],seqE[2],by=diff(seqE)/40)
        F_ <- seq(seqF[1],seqF[2],by=diff(seqF)/40)
        seqList <- list(A=A,B=B,C=C,D=D,E=E,F=F_)
        values <- rep(0,6)

        mitIndx <- which(probes.txt$Chromosome == 'chrM')

        for(j in seq(6,1)) {
            minAMAPD <- vector()
            for(k in 1:length(seqList[[j]])) {
                values[j] <- seqList[[j]][k]
                newLogR <- getNewLogR(probes.txt$Value[-mitIndx],values,ref6)
                MAPD <- median(abs(diff(newLogR)),na.rm=T)
                minAMAPD[k] <- MAPD
            }
            values[j] <- seqList[[j]][which.min(minAMAPD)]
            if(j == 1) {
                # newMAPD <- median(abs(diff(getNewLogR(logR,values,ref6))),na.rm=T)
                newLogR <- getNewLogR(probes.txt$Value[-mitIndx],values,ref6)
            }
        }
        # finalMAPD <- median(abs(diff(newLogR)),na.rm=T)
        probes.txt$Value[-mitIndx] <- newLogR

    }
    # plot(probes.txt$Value[-mitIndx],pch=16,col="#FF000020",cex=0.7,ylim=c(-2,2),main='logR')
    # points(newLogR,col="#0000FF20",cex=0.7,pch=15)



    ### Use reference file if present
    if (!is.null(referenceFile)) {
        load(referenceFile)
        ix <- !is.na(refMedians)
        if (length(refMedians)==nrow(probes.txt)) {
            probes.txt$Value[ix] <- probes.txt$Value[ix]-refMedians[ix]
        }
    }
    ### Use waviness file if present
    if (!is.null(wavinessFile)) {
        load(wavinessFile)
        ix <- !is.na(refMedians)
        if (length(refMedians)==nrow(probes.txt)) {
            l=line(refMedians,probes.txt$Value)
            probes.txt$Value=probes.txt$Value - (l$coefficients[2]*refMedians+l$coefficients[1])
        }
    }

    #browser()
    MAPD=round(median(abs(diff(probes.txt$Value)),na.rm=T),3) #This is calculated before removing outliers
    t=snps.txt$Value
    t=0.5+abs(t-0.5)
    t=t[!is.na(t)]
    t=t[t>0.9 & t<1]
    MHOF=round(median(t,na.rm=T),3)
    ix=abs(probes.txt$B.Allele.Frequency-0.5)>0.4; #ix[is.na(ix)]=F
    line=line(x=probes.txt$Value[ix],y=abs(probes.txt$B.Allele.Frequency[ix]-0.5))
    BAF.skew=round(line$coefficients[2],3)

    #browser()
    ## Data for the "TAPS" like plot

    rem=probes.txt$Chromosome=='chrX' & probes.txt$Start>85e6 & probes.txt$End<95e6 # these are pseudo autosomal and disturb interpretation

    temp.txt=probes.txt[!rem & probes.txt$Chromosome!='chrY' & probes.txt$Chromosome!='chrM',]
    #bafIx=!is.na(temp.txt$B.Allele.Frequency)
    bin=1000 #avg per bin
    if(array == 'CytoScan750K_Array') bin=1000
    laf <- avlog <- imba <- outer <- chr <- outerSD <- start <- end <- mean <- armMedian <- NULL
    for (c in unique(temp.txt$Chromosome)) if (!is.na(c)) {
        ix=temp.txt$Chromosome==c
        if(any(is.na(ix))) {
            ix[which(is.na(ix))]=F
        }
        ix <- as.logical(ix)
        tstart=temp.txt$Start[ix]
        tend=temp.txt$End[ix]
        tlog=temp.txt$Value[ix]
        tbaf=temp.txt$B.Allele.Frequency[ix]
        ix=seq(0,sum(ix),bin)
        if (length(ix)==1) ix=c(0,length(ix))
        for (i in 2:length(ix)) {
            avlog=c(avlog,median(tlog[(ix[i-1]+1):ix[i]],na.rm=T))
            mean=c(mean,mean(tlog[(ix[i-1]+1):ix[i]],na.rm=T))
            tempbaf=tbaf[(ix[i-1]+1):ix[i]]
            ai=getAI(tempbaf)
            imba=c(imba,ai[1]/ai[2])
            laf=c(laf,getLAF(tempbaf,requireMin=T)) # eventually use a fkn instead
            outer=c(outer,ai[2])
            chr=c(chr,c)
            start=c(start,tstart[ix[i-1]+1])
            end=c(end,tend[ix[i]])
            tempbaf=2*abs(tempbaf-0.5)
            outerSD=c(outerSD,sd(tempbaf[tempbaf>0.75],na.rm=T))
        }
    }
    taps.plot=data.frame(chr,start,end,avlog,mean,armMedian=NA,imba,outer,outerSD,label=substr(chr,4,7))



    #### For modelling putative combinations of purity and ploidy
    #ploidies <- seq(1,6,0.05)
    #purities <- seq(0.1,1,0.01)

    ### Mått: använd TAPS-strukturen
    ## Chromosomal missegregation score-
    ## Hur många kromosomer drabbade av hel(arm)CNA relativt medianen
    ## Chromosomal rearrangement score-
    ## Hur många kromosomer drabbade av mindre (nonCNV) CNA relativt lokal median
    armMedians=NULL
    armFrag=NULL
    for (i in 1:23) {
        chr=paste('chr',i,sep='')
        #p
        ix=taps.plot$chr==chr & taps.plot$end<chromLengths$mid[chromLengths$chr==chr]
        if (sum(ix)>2) {
            armMedians=c(armMedians,median(taps.plot$avlog[ix],na.rm=T))
            deviants=sum(abs(taps.plot$avlog[ix]-median(taps.plot$avlog[ix],na.rm=T))>0.2)
            armFrag=c(armFrag,deviants)
            taps.plot$armMedian[ix] <- median(taps.plot$avlog[ix],na.rm=T)
        }
        #q
        ix=taps.plot$chr==chr & taps.plot$start>chromLengths$mid[chromLengths$chr==chr]
        armMedians=c(armMedians,median(taps.plot$avlog[ix],na.rm=T))
        deviants=sum(abs(taps.plot$avlog[ix]-median(taps.plot$avlog[ix],na.rm=T))>0.2)
        armFrag=c(armFrag,deviants)
        taps.plot$armMedian[ix] <- median(taps.plot$avlog[ix],na.rm=T)
    }
    num.CIN=round(sum(abs(armMedians-median(armMedians))>0.05))
    str.CIN=round(sum(armFrag>0))


    #browser()
    m=median(taps.plot$armMedian[taps.plot$chr %in% paste('chr',1:22,sep='')],na.rm=T)
    #offset=mean(lr)-10.8
    #m=m-offset
    #ix=abs(taps.plot$avlog-median(taps.plot$avlog,na.rm=T))<0.3
    #if (sum(taps.plot$imba[ix]<0.1,na.rm=T)>25) ix=taps.plot$imba<0.1
    #d <- density(taps.plot$avlog,na.rm=T,bw=0.002)
    #m <- d$x[which.max(d$y)]

    probes.txt$Value=round(probes.txt$Value-m,4)
    taps.plot$avlog=taps.plot$avlog-m





    ## Smooth logratio
    cat(format(Sys.time(), "%F %H:%M:%S"),'Smoothing:',name,'\n')
    data('chromData',package='rawcopy')
    data('chromLengths',package='rawcopy')
    probes.txt$smoothed=NA
    for (chr in paste('chr',c(1:22,'X'),sep='')) {
        #cat(chr)
        ix=probes.txt$Chromosome==chr & probes.txt$Start < chromLengths$mid[chr==chromLengths$chr] & !is.na(probes.txt$Value)
        n=sum(ix)
        if (n>20) {
            kernel <- c(dnorm(seq(0, 3, length.out=40)))
            kernel <- c(kernel, rep(0, n - 2*length(kernel) + 1), rev(kernel[-1]))
            kernel <- kernel / sum(kernel)
            probes.txt$smoothed[ix] <- Re(convolve(probes.txt$Value[ix], kernel))
        }
        ix=probes.txt$Chromosome==chr & probes.txt$Start > chromLengths$mid[chr==chromLengths$chr] & !is.na(probes.txt$Value)
        n=sum(ix)
        if (n>20) {
            kernel <- c(dnorm(seq(0, 3, length.out=40)))
            kernel <- c(kernel, rep(0, n - 2*length(kernel) + 1), rev(kernel[-1]))
            kernel <- kernel / sum(kernel)
            probes.txt$smoothed[ix] <- Re(convolve(probes.txt$Value[ix], kernel))
        }
    }
    probes.txt$smoothed <- round(probes.txt$smoothed,4)


    #totals:
    profile=taps.plot$avlog[chrom_n(taps.plot$chr)<23]
    #focals:
    profile=c(profile,taps.plot$avlog-taps.plot$armMedian)
    #allelic imbalance:
    profile=c(profile,taps.plot$imba[chrom_n(taps.plot$chr)<23])
    names(profile) <- name
    cna.profile <- NULL
    cna.profile$profile=profile
    cna.profile$table=taps.plot
    save(cna.profile,file=paste(outdir,'cna.profile.Rdata',sep='/'))

    #browser()

    ## Last, produce snp profile and QC file.
    snp.profile=data.frame(snps.txt$Value[snps.txt$Quality==1])
    names(snp.profile) <- name
    save(snp.profile,file=paste(outdir,'snp.profile.Rdata',sep='/'))

    qc=data.frame(Name=name,Array=array,
                  Intensity=round(median(probes.txt$Raw.Intensity,na.rm=T),2),
                  #rawMAPD=QC$probe.MAPD,
                  MAPD=MAPD,
                  waviness=round(median(abs(diff(taps.plot$avlog)),na.rm=T),3),
                  SNPD=1-MHOF,
                  SNPskew=BAF.skew,
                  chrX=round(median(probes.txt$Value[probes.txt$Chromosome=='chrX'],na.rm=T),2),
                  chrY=round(median(probes.txt$Value[probes.txt$Chromosome=='chrY'],na.rm=T),2),
                  chrM=round(median(probes.txt$Value[probes.txt$Chromosome=='chrM'],na.rm=T),2),
                  num.CIN,str.CIN
    )
    save.txt(qc,file=paste(outdir,'qc.txt',sep='/'))

    ## Save data files including those needed for segmentation and plotting
    cat(format(Sys.time(), "%F %H:%M:%S"),'Writing:',name,'\n')

    # if (writeData) write.table(probes.txt[!is.na(probes.txt$Chromosome),],file=paste(outdir,'probes.txt',sep='/'),quote=F,row.names=F,sep='\t',na='')
    # if (writeData) write.table(snps.txt[!is.na(snps.txt$Chromosome),],file=paste(outdir,'snps.txt',sep='/'),quote=F,row.names=F,sep='\t',na='')
    save(gc,lr,raf,intensities,taps.plot,qc,probes.txt,snps.txt,file=paste(outdir,'rawcopy.Rdata',sep='/'))
}

##' Text about summary
##'
##' @param sampleFolders Samples (folders pre-processed with Rawcopy) to include.
##' @param fileName Where to store the reference file for future use.
##' @author Markus Mayrhofer, Björn Viklund
##' @export
rawcopy.makeReference <- function(sampleFolders=dir(), fileName='LocalReference.Rdata') {
    logr <- NULL
    n <- length(sampleFolders)
    for (i in 1:n) suppressWarnings( try( {
        load(paste(sampleFolders[i],'rawcopy.Rdata',sep='/'))
        logr <- cbind(logr,probes.txt$Value)
        cat(format(Sys.time(), "%F %H:%M:%S"),'loaded',sampleFolders[i],'\n')
    }, silent=T))
    n=ncol(logr)
    cat(format(Sys.time(), "%F %H:%M:%S"),'Added', n, 'samples.\n')
    chr <- probes.txt$Chromosome
    X <- chr=='chrX'; Y <- chr=='chrY'; A <- !X & !Y
    female <- male <-  NULL
    for (i in 1:n) {
        male[i] <- median(logr[Y,i],na.rm=T) > -0.25
        if (is.na(male[i])) male[i] <- F
        female[i] <- median(logr[X,i],na.rm=T) > -0.25
        if (is.na(female[i])) female[i] <- F
        if (!male[i]) logr[Y,i] <- NA
        if (!female[i]) logr[X,i] <- NA
    }
    refMedians <- apply(X = logr,MARGIN = 1,FUN = median,na.rm=T)
    refMedians[is.na(refMedians)] <- 0
    save(refMedians,file=fileName)
}

rawcopy.makeWaveinessReference <- function(sampleFolders=dir(), fileName='WavinessReference.Rdata') {
    logr <- NULL
    n <- length(sampleFolders)
    for (i in 1:n) suppressWarnings( try( {
        load(paste(sampleFolders[i],'rawcopy.Rdata',sep='/'))
        logr <- cbind(logr,probes.txt$Smoothed) ## <--------------- using smoothed data for waviness
        cat(format(Sys.time(), "%F %H:%M:%S"),'loaded',sampleFolders[i],'\n')
    }, silent=T))
    n=ncol(logr)
    cat(format(Sys.time(), "%F %H:%M:%S"),'Added', n, 'samples.\n')
    chr <- probes.txt$Chromosome
    X <- chr=='chrX'; Y <- chr=='chrY'; A <- !X & !Y
    female <- male <-  NULL
    for (i in 1:n) {
        male[i] <- median(logr[Y,i],na.rm=T) > -0.25
        if (is.na(male[i])) male[i] <- F
        female[i] <- median(logr[X,i],na.rm=T) > -0.25
        if (is.na(female[i])) female[i] <- F
        if (!male[i]) logr[Y,i] <- NA
        if (!female[i]) logr[X,i] <- NA
    }
    refMedians <- apply(X = logr,MARGIN = 1,FUN = median,na.rm=T)
    refMedians[is.na(refMedians)] <- 0
    save(refMedians,file=fileName)
}
