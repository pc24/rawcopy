



#     Copyright (C) 2015  Markus Mayrhofer, Björn Viklund
# 
#     This source code is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; version 2.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#     
#     Some data contained in this R-package are derived from files
#     provided by Affymetrix (.cdf, .cn.annot.csv and .annot.csv),
#     and which are copyrighted by Affymetrix. Redistribution and
#     modification of these data may be prohibited by Affymetrix.
#     

quant_norm <- function(data) {
    data=data+rnorm(length(data))*sd(data,na.rm=T)/1000
    data=order(order(data))
    data=qnorm(p=data/(length(data)+1))
}

getBAF <- function(raf,lr,snpClusters,allSNPs) {
    baf=rep(NA,length(raf))
    shift=snpClusters$shift
    aa.h=snpClusters$aa.h
    bb.h=snpClusters$bb.h
    ab.h=snpClusters$ab.h
    ab.h[is.na(ab.h)]=(aa.h[is.na(ab.h)]+bb.h[is.na(ab.h)])/2
    t=snpClusters$good; if (allSNPs) t[!t]=T
    for (i in which(t & !is.nan(raf) & !is.nan(lr))) {
        aa=0.5+aa.h[i]*bm(lr[i],shift[i],curve)
        ab=0.5+ab.h[i]*bm(lr[i],shift[i],curve)
        bb=0.5+bb.h[i]*bm(lr[i],shift[i],curve)
        if (!is.na(aa+ab+bb)) {
            if (raf[i]<=ab) {
                if (ab-aa > 0.1) baf[i] <- 0.5*(raf[i]-aa)/(ab-aa) 
            } else {
                if (bb-ab > 0.1) baf[i] <- 0.5+0.5*(raf[i]-ab)/(bb-ab) 
            }
        }
    }
    return(baf)
}

getMeans <- function(value,w=2) {
    means=rep(NA,length(value))
    result=rep(NA,length(value))
    n=length(means)
    sum=sum(value[1:w])
    k=w
    for (i in 1:n) {
        if (i<n-w) {
            sum=sum+value[i+w]
            k=k+1
        }
        if (i>w) {
            sum=sum-value[i-w]
            k=k-1
        }
        means[i] <- sum/k
    }
    for (i in 1:n) {
        ix=c(i-2,i+2)
        ix=ix[ix>0 & ix<=n]
        alts <- (value[i]-means[ix])
        result[i] <- alts[which.min(abs(alts))]
    }
    return(result)
}

getMedians <- function(value,w=4) {
    out=value
    start=ceiling(w/2)
    end=length(value)-(w-trunc(w-2))
    for (i in 1:start) out[i]=median(value[1:(i+start)],na.rm=T)
    for (i in end:length(value)) out[i]=median(value[(i-start+1):length(value)],na.rm=T)
    for (i in start:end) out[i]=median(value[(i-start+1):(i+start)])
    return(out)
}
medianSmooth <- function(value,w=4) {
    medians=getMedians(value,w)
    out=medians
    d1=trunc(w/2)
    d2=trunc((w-1)/2)
    start=1+d1
    end=length(out)-d2
    for (i in start:end) {
        alts=medians[c(i-d1,i+d2)]
        out[i]=alts[which.min(abs(value[i]-alts))]
    }
    return(out)
}

segMatch <- function(segments.txt,probes.txt) {
    ## Returns a data frame with probes.startIx probes.endIx
    #verry fast
    #browser()
    colnames(segments.txt)[colnames(segments.txt)=='chr']='Chromosome'
    colnames(segments.txt)[colnames(segments.txt)=='start']='Start'
    colnames(segments.txt)[colnames(segments.txt)=='end']='End'

    segStarts=data.frame(n.seg=1:nrow(segments.txt),n.pos=NA,chr=chrom_n(segments.txt$Chromosome),pos=segments.txt$Start,type='start')
    segEnds=data.frame(n.seg=1:nrow(segments.txt),n.pos=NA,chr=chrom_n(segments.txt$Chromosome),pos=segments.txt$End,type='end')
    lpos=data.frame(n.seg=NA,n.pos=1:nrow(probes.txt),chr=chrom_n(probes.txt$Chromosome),pos=(probes.txt$Start+probes.txt$End)/2,type='logr')
    dummy1=data.frame(n.seg=NA,n.pos=NA,chr=-Inf,pos=-Inf,type='logr')
    dummy2=data.frame(n.seg=NA,n.pos=NA,chr=Inf,pos=Inf,type='logr')
    
    table=rbind(segStarts,segEnds,lpos,dummy1,dummy2)
    table=table[order(table$chr,table$pos),]
    
    start=(table$type=='start')
    end=(table$type=='end')
    #l=(table$type=='logr')
    
    # omitting segment ends, let segment start "n.pos" be that of the next marker
    table$n.pos[!end][start[!end]] <- table$n.pos[!end][which(start[!end])+1]
    
    # omitting segment starts, let segment end "n.pos" be that of the previous marker
    table$n.pos[!start][end[!start]] <- table$n.pos[!start][which(end[!start])-1]
    
    # re-create a data frame with startIx and endIx (in probes.txt) per segment
    indices=data.frame(startIx=table$n.pos[start],endIx=table$n.pos[end])
    
    na=is.na(indices$startIx+indices$endIx)
    na[!na] = indices$startIx[!na]>indices$endIx[!na]
    indices$startIx[na] <- indices$endIx[na] <- NA
    
    return(indices)
}
getAI <- function(baf,min=20,seed=NULL) {#,divide=T) {
    baf=baf[!is.na(baf)]
    if (length(baf)<min) return(c(NA,NA))
    baf=2*abs(baf-0.5)
    set.seed(seed)
    km=kmeans(x=baf,centers=2)
    het=min(km$centers)
    hom=max(km$centers)
    if (min(km$size)<3) return (c(NA,NA))
    #if (divide) return(het/hom) else return(het)
    return(c(het,hom))
}
getLAF <- function(baf,min=200,requireMin=T) { ### Inte klar! 
    baf=baf[!is.na(baf)]
    baf=abs(baf-0.5)
    baf=sort(baf)
    ix=seq(1,length(baf),by=2)
    baf[ix]=baf[ix]+0.5
    baf[-ix]= -baf[-ix]+0.5
    if (requireMin & length(baf)<min) return(NA)
    d=density(baf,adjust=0.2)
    t=c(F,diff(sign(diff(d$y)))<0,F)
    t[is.na(t)]=F; t=which(t)
    hom_height=mean(d$y[t[1]],d$y[t[length(t)]])
    #t=trim(t)
    peaks=d$x[t]
    peaks=peaks[d$y[t]>=0.15*hom_height]
    peaks=(0.5-abs(peaks-0.5))
    outer=1-mean(peaks[c(1,length(peaks))])
    scale=1/outer
    peaks=1-scale*(1-peaks)
    if (length(peaks)<3) return(0)
    if (length(peaks)==3) return(trim(peaks))
    if (length(peaks)==4) return(mean(trim(peaks)))
    return(NA)
}

addGenes <- function(data,genes) {
    if (nrow(data)==0) return(data)
    data$genes=''
    for (i in 1:nrow(data)) {
        ix=which(genes$chrom==as.character(data$Chromosome[i]) & genes$txStart<data$End[i] & genes$txEnd>data$Start[i])
        if (length(ix)>0) data$genes[i]=paste(sort(unique(genes$name2[ix])),collapse=', ')
    }
    return(data)
}

chrom_ucsc <- function(data) {
    out=rep(NA,length(data))
    c=as.character(unique(data))
    for (i in 1:length(c)) {
        temp=c[i]
        if (temp=='23') temp='X'
        if (temp=='24') temp='Y'
        if (temp=='MT') temp='M'
        if (temp %in% c(1:22,'X','Y','M')) out[data==c[i]]=paste('chr',temp, sep='')
    }
    return(out)
}

deChrom_ucsc <- function(data) {
    ##  chroms in 1:24 and chr1:chrY format conversion
    if (length(data)==0) return (data)
    keep_index=NULL
    CHR=rep(0,length(data))
    existing=unique(data)
    for (i in 1:length(existing))   {
        temp=strsplit(as.character(existing[i]),'_')
        temp=strsplit(temp[[1]],'chr')[[1]][2]
        if (temp=='X') temp='23'
        if (temp=='_X') temp='23'
        if (temp=='Y') temp='24'
        CHR[data==existing[i]]=as.integer(temp)
    }
    return (CHR)
}

chrom_n <- function(data) {
    out=rep(Inf,length(data))
    for (c in c(1:22)) {
        out[data==paste('chr',c, sep='')]=c
    }
    out[data=='chrX']=23
    out[data=='chrY']=24
    out[data=='chrM']=25
    return(out)
}

## Funktion för samband mellan hybridisering per snp och rawBAF.

curve <- structure(list(x = c(7, 7.1, 7.2, 7.3, 7.4, 7.5, 7.6, 7.7, 7.8, 
                              7.9, 8, 8.1, 8.2, 8.3, 8.4, 8.5, 8.6, 8.7, 8.8, 8.9, 9, 9.1, 
                              9.2, 9.3, 9.4, 9.5, 9.6, 9.7, 9.8, 9.9, 10, 10.1, 10.2, 10.3, 
                              10.4, 10.5, 10.6, 10.7, 10.8, 10.9, 11, 11.1, 11.2, 11.3, 11.4, 
                              11.5, 11.6, 11.7, 11.8, 11.9, 12, 12.1, 12.2, 12.3, 12.4, 12.5, 
                              12.6, 12.7, 12.8, 12.9, 13, 13.1, 13.2, 13.3, 13.4, 13.5, 13.6, 
                              13.7, 13.8, 13.9, 14), y = c(0.0416335227664092, 0.050979763640533, 
                                                           0.0603961238329525, 0.0699257007683986, 0.0796069246141624, 0.089472319342666, 
                                                           0.0995474941172804, 0.109850392141516, 0.120390812524894, 0.13117020888403, 
                                                           0.142181756918978, 0.153410672653813, 0.164834753905488, 0.176425110239248, 
                                                           0.188147041446085, 0.199961021562655, 0.21182374463445, 0.223689189665498, 
                                                           0.235509665271151, 0.247236799154316, 0.258822443319578, 0.270219472568468, 
                                                           0.281382460931114, 0.292268227947705, 0.302836253800124, 0.313048968911182, 
                                                           0.322871929495096, 0.332273895395454, 0.341226830146697, 0.349705845339663, 
                                                           0.357689111913343, 0.365157759865717, 0.37209578511123, 0.378489977969992, 
                                                           0.384329882346757, 0.389607788468662, 0.394318755630878, 0.398460655348789, 
                                                           0.402034220248617, 0.405043080508809, 0.407493768136766, 0.409395670095172, 
                                                           0.410760914320152, 0.411604177795414, 0.411942412619328, 0.411942412619328, 
                                                           0.411942412619328, 0.411942412619328, 0.411942412619328, 0.411942412619328, 
                                                           0.411942412619328, 0.411942412619328, 0.411942412619328, 0.411942412619328, 
                                                           0.411942412619328, 0.411942412619328, 0.411942412619328, 0.411942412619328, 
                                                           0.411942412619328, 0.411942412619328, 0.411942412619328, 0.411942412619328, 
                                                           0.411942412619328, 0.411942412619328, 0.411942412619328, 0.411942412619328, 
                                                           0.411942412619328, 0.411942412619328, 0.411942412619328, 0.411942412619328, 
                                                           0.411942412619328)), .Names = c("x", "y"), row.names = c(NA, 
                                                                                                                    -71L), class = "data.frame")


curve_old <- structure(list(x = c(7, 7.1, 7.2, 7.3, 7.4, 7.5, 7.6, 7.7, 7.8, 
                              7.9, 8, 8.1, 8.2, 8.3, 8.4, 8.5, 8.6, 8.7, 8.8, 8.9, 9, 9.1, 
                              9.2, 9.3, 9.4, 9.5, 9.6, 9.7, 9.8, 9.9, 10, 10.1, 10.2, 10.3, 
                              10.4, 10.5, 10.6, 10.7, 10.8, 10.9, 11, 11.1, 11.2, 11.3, 11.4, 
                              11.5, 11.6, 11.7, 11.8, 11.9, 12, 12.1, 12.2, 12.3, 12.4, 12.5, 
                              12.6, 12.7, 12.8, 12.9, 13, 13.1, 13.2, 13.3, 13.4, 13.5, 13.6, 
                              13.7, 13.8, 13.9, 14), y = c(0.0416335227664092, 0.050979763640533, 
                                                           0.0603961238329525, 0.0699257007683986, 0.0796069246141624, 0.089472319342666, 
                                                           0.0995474941172804, 0.109850392141516, 0.120390812524894, 0.13117020888403, 
                                                           0.142181756918978, 0.153410672653813, 0.164834753905488, 0.176425110239248, 
                                                           0.188147041446085, 0.199961021562655, 0.21182374463445, 0.223689189665498, 
                                                           0.235509665271151, 0.247236799154316, 0.258822443319578, 0.270219472568468, 
                                                           0.281382460931114, 0.292268227947705, 0.302836253800124, 0.313048968911182, 
                                                           0.322871929495096, 0.332273895395454, 0.341226830146697, 0.349705845339663, 
                                                           0.357689111913343, 0.365157759865717, 0.37209578511123, 0.378489977969992, 
                                                           0.384329882346757, 0.389607788468662, 0.394318755630878, 0.398460655348789, 
                                                           0.402034220248617, 0.405043080508809, 0.407493768136766, 0.409395670095172, 
                                                           0.410760914320152, 0.411604177795414, 0.411942412619328, 0.411794493781286, 
                                                           0.411180800367166, 0.410122749303893, 0.40864230673061, 0.406761505977464, 
                                                           0.404502002477085, 0.40188469452177, 0.398929434687548, 0.395654850324527, 
                                                           0.392078283345505, 0.388215850385281, 0.384082615093144, 0.379692855703378, 
                                                           0.375060403862655, 0.370199025585281, 0.365122812558449, 0.359846551998482, 
                                                           0.354386045797419, 0.348758354510559, 0.342981948351701, 0.337076755183574, 
                                                           0.331064103842254, 0.324966569326709, 0.318807733770764, 0.312611883139102, 
                                                           0.306403663826835)), .Names = c("x", "y"), row.names = c(NA, 
                                                                                                                    -71L), class = "data.frame")


bm <- function(X, shift=0,curve) {
    X=X-shift
    X=round(X,1)
    Y=curve$y[match(X,curve$x)]
    return(Y)
}
## Används i makeReferenceData för euklidiska avstånd mellan punkter och kurva
getResiduals <- function(xpoints, ypoints, func, xmin=7, xmax=13) {
  xvals=seq(xmin,xmax,0.1)
  yvals=func(xvals)
  dists=rep(NA,length(xpoints))
  for (i in 1:length(dists)) dists[i]=min(sqrt( (xpoints[i]-xvals)^2+(ypoints[i]-yvals)^2 ) ,na.rm=T)
  return(dists)
}


getSqrt <- function(snpix,sampleix) {
  return(round(log2(median(sqrt(snps[snpix,ixa[sampleix]]^2+snps[snpix,ixb[sampleix]]^2))),2))
}

outliers <- function(ta, tb, cut=0.03) {
  r <- sqrt(ta^2+tb^2)
  q <- abs(1-r/median(r))
  return(q>cut)
}

## Function for regionMean calculation (fragment GC content).
regionMeans <- function(s,e,pos,values) {
  means <- rep(NA,length(s))
  temp=data.frame(pos=c(pos,s,e),value=c(values,rep(NA,2*length(s))))
  order=order(order(as.integer(temp$pos))) #order: vart i sorteradtemp sitter respektive värde i temp
  spositions=order[(length(pos)+1) : (length(pos)+length(s))] # vart i sorterad temp sitter respektive "s"
  epositions=order[(length(pos)+1+length(s)) : nrow(temp)]
  temp=temp[order(as.integer(temp$pos)),2] #temp är numera bara sorterade values, med NA för de pos som är s eller e
  for (i in 1:length(means)) {
    means[i]=mean(temp[spositions[i]:epositions[i]],na.rm=T)
  }
  return(means)
}

## Function for normalization
norm <- function(data, factor1, window=0.05,trim=1) {
  n=length(data)
  range1=c(round(seq(1,n,window*n)),n)
  o1=order(factor1)
  data2=data
  for (i in 2:length(range1)) {
    ix1=o1[range1[i-1]:range1[i]]
    ix1=ix1[!is.na(ix1)]
    d=data[ix1]
    #if (length(d)>2*trim) mid=mean(d[order(d)[(1+trim):(length(d)-trim)]],na.rm=T) else mid=mean(d,na.rm=T)
    if (length(ix1)>1) data2[ix1]=d-median(d,na.rm=T)
  }
  return(data2)
}
norm2 <- function(data, factor1, factor2, window=0.05,trim=1) {
  n=length(data)
  range1=c(round(seq(1,n,window*n)),n)
  range2=c(round(seq(1,n,window*n)),n)
  o1=order(factor1)
  o2=order(factor2)
  data2=data
  for (i in 2:length(range1)) {
    ix1=o1[range1[i-1]:range1[i]]
    ix1=ix1[!is.na(ix1)]
    for (j in 2:length(range2)) {
      ix2=o2[range2[j-1]:range2[j]]
      ix2=ix2[!is.na(ix2)]
      ix12 = intersect(ix1, ix2)
      d=data[ix12]
      #if (length(d)>2*trim) mid=mean(d[order(d)[(1+trim):(length(d)-trim)]],na.rm=T) else mid=mean(d,na.rm=T)
      if (length(ix12)>1) data2[ix12]=d-median(d,na.rm=T)
    }
  }
  return(data2)
}
norm3 <- function(data, factor1, factor2, factor3, window=0.05,trim=1) {
  n=length(data)
  range1=c(round(seq(1,n,window*n)),n)
  range2=c(round(seq(1,n,window*n)),n)
  range3=c(round(seq(1,n,window*n)),n)
  o1=order(factor1)
  o2=order(factor2)
  o3=order(factor3)
  data2=data
  for (i in 2:length(range1)) {
    #print(i)
    #browser()
    ix1=o1[range1[i-1]:range1[i]]
    ix1=ix1[!is.na(ix1)]
    for (j in 2:length(range2)) {
      ix2=o2[range2[j-1]:range2[j]]
      ix2=ix2[!is.na(ix2)]
      ix12 = intersect(ix1, ix2)
      for (k in 2:length(range3)) {
        ix3=o3[range3[k-1]:range3[k]]
        ix3=ix3[!is.na(ix3)]
        ix123 = intersect(ix12, ix3)
        d=data[ix123]
        #browser()
        #if (length(d)>2*trim) mid=mean(d[order(d)[(1+trim):(length(d)-trim)]],na.rm=T) else mid=mean(d,na.rm=T)
        if (length(ix123)>1) data2[ix123]=d-median(d,na.rm=T)
        #if (i==5 & j==5) browser()
      }
    }
  }
  return(data2)
}

## Remove first and last few values of a vector
trim <- function(vector, n=1, end=NULL) {
  length=length(vector)    
  if (is.null(end)) end=n
  if (length(vector) <= n+end) return(numeric(0))
  return(vector[(n+1):(length-end)])
}

save.txt <- function(data,file='temp', row.names=F, col.names=T, append=F, sep='\t') {   ## simplified for convenience
    write.table(data,file=file,sep=sep,quote=F,row.names=row.names, col.names=col.names, append=append)
}
load.txt <- function(file,sep='\t') { ## just to be rid of "sep"
    read.csv(file,sep=sep) 
}



colorFunction <- function(n) {
    black <- 0
    #black2 <- 60
    red <- 0.17
    #orange <- 342
    yellow <- 0.3
    white <- 0.37
    white2 <- .4
    factor <- 2.247e3
    factor <- 1.247e3
    factor <- 1e3

    # black2Red <- colorRampPalette(c('black','red'))((red - black)*factor) #black2
    black2Red <- colorRampPalette(c('black','#700000','#800000','#900000','#950000','red3','red2','red'))((red - black)*factor)
    redYellow <- colorRampPalette(c('red','yellow'))((yellow - red)*factor)
    yellowWhite <- colorRampPalette(c('yellow','white'))((white - yellow)*factor)
    whiteWhite1 <- colorRampPalette(c('white','white'))((white2 - white)*factor)
    # print(n)
    # length(c(black2Red,redYellow,yellowWhite,whiteWhite1))

    return(c(black2Red,redYellow,yellowWhite,whiteWhite1))
    # return(c(black2Red,redYellow,yellowWhite,whiteWhite1)[1:n])
}

getNewLogR <- function(logR,values,ref6) {
    return(logR - (values[1] * ref6$ka + 
                   values[2] * ref6$kb +  
                   values[3] * ref6$kc + 
                   values[4] * ref6$kd + 
                   values[5] * ref6$ke +  
                   values[6] * ref6$kf + 
                   ref6$m)
    )
}

regsFromSegs <- function (Log2,alf, segments, bin=150,min=1,matched=F,allelePeaks=FALSE) {
    ## This function builds short segments and calcualtes their average Log-R and Allelic Imbalance Ratio.
    rownames(Log2)=1:nrow(Log2)
    rownames(alf)=1:nrow(alf)
    regs=NULL #list('chr'=NULL,'start'=NULL,'end'=NULL,'logs'=NULL,'scores'=NULL,'probes'=NULL,'snps'=NULL)
    #,'key1'=rep(NA,nrow(Log2)),'key2'=rep(NA,nrow(alf)))
    n=nrow(segments)
    s_check=NULL
    
    pMatch=segMatch(segments,Log2)
    sMatch=segMatch(segments,alf)
    
    for (c in 1:n) { ## for every segment
        #tlog=Log2[Log2$Chromosome==segments$Chromosome[c],] ## Log-R on this chromosome
        #talf=alf[alf$Chromosome==segments$Chromosome[c],] ## Allele Freq on this chromosome
        #tlog=tlog[(tlog$Start>=segments$Start[c])&(tlog$Start<segments$End[c]),] ## Log-R on this segment
        #talf=talf[(talf$Start>=segments$Start[c])&(talf$Start<segments$End[c]),] ## Allele Freq on this segment
        
        tsnps=sMatch[c,2]-sMatch[c,1]+1; if (is.na(tsnps)) tsnps=0   #nrow(talf)    ## number of snps and probes in this segment
        tprobes=pMatch[c,2]-pMatch[c,1]+1; if (is.na(tsnps)) tsnps=0   #nrow(tlog)
        tnregs=max(trunc(tsnps/bin),1) ## Further split into how many (a minimum of 1) subsegments
        tcuts=segments$Start[c] ## The first start pos
        tlength=segments$End[c]-segments$Start[c]    ## Length of this whole segment
        for (i in 1:tnregs) tcuts = c(tcuts, tcuts[1]+round(i/tnregs*tlength)) ## Break the segment at these positions
        
        tab=data.frame(chr=segments$Chromosome[c],start=tcuts[-length(tcuts)],end=tcuts[-1],logs=NA,scores=NA,probes=0,snps=0)
        if (is.null(regs)) regs=tab else regs=rbind(regs,tab)
    }
    
    pMatch=segMatch(regs,Log2)
    sMatch=segMatch(regs,alf)
    
    for (i in 1:nrow(regs)) {    ## build the subsegments
        #regs$chr=c(regs$chr,as.character(segments$Chromosome[c]))    ## Chromosome
        #s_=tcuts[r]                                                     ## Start
        #e_=tcuts[r+1]                                                 ## End
        #thisalf=talf[(talf$Start>=s_)&(talf$Start<=e_),]             ## get the Log-R values
        #thislog=tlog[(tlog$Start>=s_)&(tlog$Start<=e_),]             ## and the allele frequency
        #regs$key1[as.integer(rownames(thislog))]=length(regs$chr)    ## store their positions for fast access during plotting
        #regs$key2[as.integer(rownames(thisalf))]=length(regs$chr)    ## --"--
        if (!is.na(pMatch[i,1])) { 
            regs$logs[i]=median(Log2$Value[pMatch[i,1]:pMatch[i,2]],na.rm=T)  ## store average log ratio of this segment
            regs$probes[i]=pMatch[i,2]-pMatch[i,1]+1                   ## store number of probes
        }
        if (!is.na(sMatch[i,1])) { 
            temp = NA
            try(temp <- allelicImbalance(alf$Value[sMatch[i,1]:sMatch[i,2]],min,matched,allelePeaks),silent=T)
            regs$scores[i]=temp
            regs$snps[i]=sMatch[i,2]-sMatch[i,1]+1                       
        }
    }
    
    #regs=as.data.frame(regs)
    regs=regs[!is.na(regs$logs),]  ### MODDAT MARKUS MAJ 2013
    return (regs)
}
allelicImbalance <- function (data,min=30,matched=F,allelePeaks=F) {
    #browser()
    if(matched == T ) {
        return(2*median(abs(data-.5),na.rm=T))
    }
    if(allelePeaks == F) {
        if (length(data)>min) {                                    ## Time to calculate Allelic Imbalance Ratio (if enough SNPs)
            t1=sort( abs(data-0.5) )                            ## distance from middle (het) in the allele freq pattern, ascending
            if (length(unique(t1))>3) {                                ## do not attempt clustering with too few snps
                xx=NULL
                try(xx <- kmeans(t1, 2),silent=T)                            ## Attempt k-means (Hartigan-Wong: has proven very stable)
                if (!is.null(xx)) if (min(xx$size) > 0.05*max(xx$size)) {    ## On some occations data quality is poor, requiring 5%+ heterozygous SNPs avoids most such cases.
                    xx=xx$centers
                } else xx=NA
            } else xx=NA      
        } else xx=NA
        #try (if (is.na(xx)) xx=0:1, silent=T)
        #try (if (length(xx)==0) xx=0:1, silent=T)
        #regs$scores=c(regs$scores, min(xx)/max(xx) )                ## Allelic Imbalance Ratio = inner / outer cluster.
        #regs$het=c(regs$het, min(xx))                                ## $het and $hom are no longer in use.
        #regs$hom=c(regs$hom, max(xx))
        return( min(xx)/max(xx) )
    }
    #The new shiet
    if(allelePeaks == T) {
        return(1-max(kmeans(data,2)$centers))
    }
}



pileup <- function(regions) { #, nsamples) {

    colnames(regions)[colnames(regions)=='chr']='Chromosome'
    colnames(regions)[colnames(regions)=='start']='Start'
    colnames(regions)[colnames(regions)=='end']='End'
    regions <- regions[!is.na(regions$Chromosome),]

    if (nrow(regions)==0) return(data.frame(Chromosome=integer(0),Start=integer(0),End=integer(0),Count=integer(0)))
    chrs <- unique(as.character(regions$Chromosome))
    if (length(chrs)>1) { # if more than one chrom, run them separately.
        pile=NULL
        for (i in 1:length(chrs)) pile <- rbind(pile,pileup(regions[as.character(regions$Chromosome)==chrs[i],]))
        #pile$Percent <- round(100*pile$Count/nsamples) # this is done once.
        return(pile[order(pile$Chromosome),])
    } else {
        c=as.character(regions$Chromosome[1])
        starts=data.frame(Chromosome=c,Start=regions$Start,End=regions$Start,type='start')
        ends=data.frame(Chromosome=c,Start=regions$End,End=regions$End,type='end')
        pile <- rbind(starts,ends)
        pile <- pile[order(pile$Start),]
        pile$Count <- 1
        
        for (i in 2:nrow(pile)) {
            pile$Count[i] <- pile$Count[i-1] + ifelse(pile$type[i]=='start',1,-1)
            pile$End[i-1] <- pile$Start[i]
        }
        pile <- pile[(pile$End > pile$Start) & (pile$Count > 0),]
        return(pile)
    }
}
