# ____  _   _ ____      
#/ ___|| \ | |  _ \ ___ 
#\___ \|  \| | |_) / __|
# ___) | |\  |  __/\__ \
#|____/|_| \_|_|   |___/
#                       
#SNPs
################## Process SNPs into clusters and stats.

# load('data/snps.Rdata') #Läs in rätt snps-fil
# load('/media/safe/bjorn/git/rawcopy/rawcopyReference/data/snps.Rdata')
load('/media/safe/bjorn/CEL.SNP6/newSnps.Rdata')
# load('data/newSnps.Rdata')

## CytoScan block
setwd('/media/safe/bjorn/git/rawcopy/rawcopyReference')
# load('data/CytoScanHD_snps_2014-07-01_13:24:28.Rdata')
# load('data/CytoScanHD_annot_2014-06-24_14:38:01.Rdata')
load('data/CytoScanHD_snps_2014-08-11_13_07_14.Rdata')
load('data/CytoScanHD_annot_2014-08-18_14_04_53.Rdata')
newSnps <- snps
rm(snps)
snpData=newSnps[,1:2]
snps=newSnps[,-(1:2)]
rm(newSnps)
gc()
###

source('functions.r')


snpData=newSnps[,1:2]
snps=newSnps[,-(1:2)]
rm(newSnps)
gc()
ixa=seq(1,ncol(snps),2)
ixb=ixa+1
gc()

### Load snp annotation and match it with snp data
# load('data/annot.Rdata')
load("/media/safe/bjorn/git/rawcopy/rawcopyReference/data/annot.Rdata")
# load('data/CytoScanHD_annot_2014-06-24_14:38:01.Rdata')
annot=merge(snpData,annot,by=1,all.x=T,all.y=F)
annot=annot[order(annot$ix),]


gc()

### Calculate total snp intensities
snpsums <- log2(sqrt(snps[,ixa]^2+snps[,ixb]^2))
gc()

### QC!!! Skip and load if lazy.
sampleData=data.frame(sample=colnames(snpsums)) 

sampleData$snp.nsp=NA
sampleData$snp.sty=NA
sampleData$snp.both=NA
sampleData$snp.MAPD=NA
sampleData$snp.X=NA
sampleData$snp.Y=NA
sampleData$snp.MT=NA

source('/media/safe/bjorn/git/rawcopy/rawcopyPackage/R/functions.r')

for (s in 1:nrow(sampleData)) {    
    # print(paste(s,'/',nrow(sampleData),sep=''))
    sampleData$snp.nsp[s]=getSqrt(!annot$sty & annot$Chromosome=='1',s)
    sampleData$snp.sty[s]=getSqrt(!annot$nsp & annot$Chromosome=='1',s)
    sampleData$snp.both[s]=getSqrt(annot$nsp & annot$sty & annot$Chromosome=='1',s)
    sampleData$snp.X[s]=getSqrt(annot$nsp & annot$Chromosome=='X',s) - 
        getSqrt(annot$nsp & annot$Chromosome=='1',s)
    sampleData$snp.Y[s]=getSqrt(annot$nsp & annot$Chromosome=='Y',s) - 
        getSqrt(annot$nsp & annot$Chromosome=='1',s)
    sampleData$snp.MT[s]=getSqrt(annot$Chromosome=='MT',s) - 
        getSqrt(annot$Chromosome=='1',s)
    ix=which(annot$Chromosome=='1' & !annot$sty)
    ix=ix[order(annot$Physical.Position[ix])]
    sampleData$snp.MAPD[s]=round(median(abs(diff(snpsums[,s]))),3)
    print(sampleData[s,])
}


#save(sampleData,file='data/sampleDataS.Rdata')
#load('data/sampleDataS.Rdata')


type=rep(4,nrow(snps))
type[!annot$nsp]=3
type[!annot$sty]=2


### Kolla fôrmen på kûrvan
# temp=c('VENUE_p_TCGAb28_SNP_N_GenomeWideSNP_6_D09_568984.CEL',
#        'GRIPS_p_TCGA_b116_SNP_N_GenomeWideSNP_6_F03_781496.CEL',
#        'BAIZE_p_TCGA_b138_SNP_N_GenomeWideSNP_6_B09_808858.CEL')
# ix=c(which(sampleData$sample %in% temp))
# sampleData[ix,]
# t=1:nrow(snps)
# baf=abs(as.matrix(snps[t,ixb[ix]]/(snps[t,ixa[ix]]+snps[t,ixb[ix]]),ncol=1)-0.5)
# plot(
#     as.vector(snpsums[t,ix]),
#     baf,
#     xlim=c(7,13),ylim=c(-0,1),
#     xlab='snp total',
#     ylab='BAF',
#     col='#00000003',
#     pch=20
# )
# bafFunc <- data.frame(x=seq(8,13,.1),y=NA)
# for(i in 1:nrow(bafFunc)) {
#     m=as.vector(snpsums[t,ix])
#     m=baf[abs(m-bafFunc$x[i])<.15]
#     m=density(m)
#     m=m$x[order(m$y,decreasing=T)][1]
#     bafFunc$y[i]=m
# }
# bafFunc$y=bafFunc$y/(max(bafFunc$y)/0.5)
# 
# lines(bafFunc$x,bafFunc$y,col='pink')
# 
# bafModel=lm(bafFunc$y ~ bafFunc$x + I(bafFunc$x^2))
# 
# plot(bafFunc$x,bafFunc$y)
# x=5:15
# lines(x=x,y=bafModel$coefficients[3]*x^2 + bafModel$coefficients[2]*x+bafModel$coefficients[1])
# 

# Funktion för samband mellan hybridisering och B-allelfrekvens
# bm <- function(x,shift=0) { ## Måste hårdkoda koefficienterna nedan så småningom. Klart.
#     x_=x-shift
#     y= -0.03654366*x_^2 + 0.8197303*x_ -4.087099
#     y[y<0]=NA
#     names(y)=x
#     return(y)
# } 
 

### plot all MT&Y from males
# mty=annot$Chromosome=='MT' | annot$Chromosome=='Y'
# male=sampleData$snp.X < -0.4
# plot(
#     as.vector(snpsums[mty,male]),
#     as.vector(as.matrix(snps[mty,ixb[male]]/(snps[mty,ixa[male]]+snps[mty,ixb[male]]))),
#     xlim=c(7,15),ylim=c(-0,1),
#     xlab='snp total',
#     ylab='BAF',
#     col='#00000003',
#     pch=20
# )


################ Process SNP BAFs and Values

## Investigate snp intensities
par(mfrow=c(2,3),mar=c(4,4,1,1))
plot(sampleData$snp.nsp,sampleData$snp.MAPD,pch=20,col='#00000040',xlab='NSP snp hybridization',ylab='NSP probe MAPD')
plot(sampleData$snp.nsp,sampleData$snp.X,pch=20,col='#00000040',xlab='NSP snp hybridization',ylab='NSP X')
plot(sampleData$snp.nsp,sampleData$snp.Y,pch=20,col='#00000040',xlab='NSP snp hybridization',ylab='NSP Y')
plot(sampleData$snp.MAPD,sampleData$snp.X,pch=20,col='#00000040',xlab='NSP snp MAPD',ylab='NSP X')
plot(sampleData$snp.MAPD,sampleData$snp.Y,pch=20,col='#00000040',xlab='NSP snp MAPD',ylab='NSP Y')
dev.off()

## Mark samples unsuitable for snp "LOG-R" reference data
# CytoScan BAD
bad=sampleData$snp.nsp < 9.5 |
    sampleData$snp.X < -0.75 |
    sampleData$snp.MAPD > 0.9 |
    ((sampleData$snp.X > -0.5) & (sampleData$snp.X < -0.2))
G= !bad

##
bad=sampleData$snp.nsp < 9 |
    sampleData$snp.X < -0.75 |
    sampleData$snp.MAPD > 0.8 |
    ((sampleData$snp.X > -0.5) & (sampleData$snp.X < -0.2))
G= !bad


## Indices over which SNPs are Autosomal, C, Y, MY and which samples are considered male.
tA=(annot$Chromosome=='X' & (annot$ChrX.pseudo.autosomal.region.1==1 | annot$ChrX.pseudo.autosomal.region.2==1) |
        (annot$Chromosome!='X' & annot$Chromosome!='Y' & annot$Chromosome!='MT'))
tX=(annot$Chromosome=='X' & annot$ChrX.pseudo.autosomal.region.1==0 & annot$ChrX.pseudo.autosomal.region.2==0)
tY=(annot$Chromosome=='Y')
tMT=(annot$Chromosome=='MT')
male=sampleData$snp.X < -0.4

snpClusters=snpData #Reference data will be stored here
good  <- shift <- bb.h <- bb.median <- ab.h <- ab.median <- aa.h <- aa.median <- 
    k <- m <- haploid.k <- haploid.m <- absent.k <- absent.m <- meanResidual <- rep(NA,nrow(snpClusters))

save.image(file=paste("data/CytoScanHD_SNPs_beforeLoop_",format(Sys.time(), "%F-%H_%M_%S"),".Rdata",sep=''))
save.image(file=paste("data/SNP6_SNPs_beforeLoop_",format(Sys.time(), "%F-%H_%M_%S"),".Rdata",sep=''))

#Ska vi plotta?
plot=T; if (plot) par(mfrow=1:2,mar=c(4,4,1,1))
# ____  _                                    _                       __       _ _       
#/ ___|| |_ ___  _ __     _ __ ___  __ _  __| |   ___ __ _ _ __ ___ / _|_   _| | |_   _ 
#\___ \| __/ _ \| '_ \   | '__/ _ \/ _` |/ _` |  / __/ _` | '__/ _ \ |_| | | | | | | | |
# ___) | || (_) | |_) |  | | |  __/ (_| | (_| | | (_| (_| | | |  __/  _| |_| | | | |_| |
#|____/ \__\___/| .__( ) |_|  \___|\__,_|\__,_|  \___\__,_|_|  \___|_|  \__,_|_|_|\__, |
#               |_|  |/                                                           |___/ 
#Stop, read carefully

# This for loop is moved to the file getSNPfunctions.R because it is very ram intensive The function createSNPfunctions
# is more updated than the for loop in this file. It is left untouched only because it plotted a picture to the manuscript. 
# getSNPfunctions.R splits all the probes into 3 equal sized parts, and will run it separately. 

##!/bin/bash
# /usr/bin/Rscript getSNPfunctions.R 1
# /usr/bin/Rscript getSNPfunctions.R 2
# /usr/bin/Rscript getSNPfunctions.R 3

# Dont forget to rbind the results afterwards 

#################### Skip from here

source('/media/safe/bjorn/git/rawcopy/rawcopyPackage/R/functions.r')
# Stora loopen börjar... nu!
# Testa 1:1000
    # for (i in c(17,20)) {
for (i in 1:nrow(snpClusters)) {
    plot<-T
    
    if (trunc(i/1e1)==i/1e1) cat(i,'\n')   
    
    ## Temp vectors for current raw baf and log intensity
    if (tA[i] | tMT[i]) { # autosomal or mitochondrial SNP, use all samples
        baf=as.numeric(snps[i,ixb]/(snps[i,ixa]+snps[i,ixb]))
        val=snpsums[i,] 
    }
    
    if (tX[i]) { # X chrom, use female only
        baf=as.numeric(snps[i,ixb[!male]]/(snps[i,ixa[!male]]+snps[i,ixb[!male]]))
        val=snpsums[i,!male] 
    }
    
    if (tY[i]) { # Y chrom, use male only
        baf=as.numeric(snps[i,ixb[male]]/(snps[i,ixa[male]]+snps[i,ixb[male]]))
        val=snpsums[i,male] 
    }
    
    km=NULL # the k means clustering object
    
    if (tA[i] | tX[i]) try( { # if autosomal or X, seek 3 clusters
        km=kmeans(baf,c(0.2,.5,.8))
    }, silent=T)
    if (tY[i] | tMT[i]) try( { # if Y or MT, seek 2 clusters
        km=kmeans(baf,c(0.3,.7))
    }, silent=T)
    
#     if (is.null(km)) try( { ### Tillagt September -14
#         km=kmeans(baf,2)    ### Om inte önskad 3-means eller 2-means funkade, försök med en enklare 2-means
#     }, silent=T)
    
    if (plot) try( {
        cat('totss',round(km$totss/length(km$cluster),3),'\n')
        cat('betweenss',round(km$betweenss/length(km$cluster),3),'\n')
        cat('clusters:',length(km$centers),'\n')
    }, silent=T)
    
    col='#00000050'; if (!is.null(km)) col=km$cluster
    #Good = 21
    #Bad  = 20
    if (plot) {

        pdf(paste('/media/safe/bjorn/2014/rawcopyImages/cytoscan_ballele_with_dots_nolines_', format(Sys.time(), "%F-%H_%M_%S"),'.pdf',sep=''),width=5,height=5)
        # pdf(paste('/media/safe/bjorn/2014/rawcopyImages/cytoscan_ballele_with_dots_bad.pdf',sep=''),width=5,height=5)
        par(mar=c(3,3,3,3),xaxs='i',yaxs='i',lend=1,las=1)
        plot(val,baf,xlim=c(8,12),ylim=c(-0,1),xlab='snp total', ylab='BAF', col='black',pch=21,bg='#00000030',cex=1.2,axes=F)
        # plot(val,baf,xlim=c(8,12),ylim=c(-0,1),xlab='snp total', ylab='BAF', col='black',pch=21,bg='#00000030',cex=1.2,axes=F,type='n')
        axis(1,lwd=2,cex.axis=1.5)
        axis(2,lwd=2,cex.axis=1.5)
        box(lwd=2)
        grid(col='#00000040',lwd=1.5,lty=2)

    }
    OK= !is.null(km)
    if (OK) OK= km$totss/length(km$cluster)>0.018
    
    lwdLine <- 3
    colLine <- 'black'
       
    good=F
    if (OK) { 
        
        good=T
        
        ## Build SNP descriptor        
        ## Find parameters h and shift that optimizes the bafFunc+0.5
        order=order(km$size,decreasing=T) # order the clusters on size
        if (length(order)==3 & order[1]==2) order[1:2]=order[2:1] # if middle cluster is largest, place it 2nd anyway (bad shift estimator)
        if (length(order)==2 & # if only 2 clusters, place the smaller cluster first if bigger cluster is near 0.5 (bad shift estimator)
                abs(0.5-km$centers[order[1]])<0.1 &
                abs(0.5-km$centers[order[1]])<abs(0.5-km$centers[order[2]])) 
            order[1:2]=order[2:1]
        shift=NA # BAF function position on x axis
        for (cl in order) {
            ix=km$cluster==cl 
            tbaf=baf[ix] # baf of this cluster
            tval=val[ix] # values of this cluster
            
            ## optimize BAF function height and shift so that tbaf ~ 0.5+h*bm(tval+shift)
            shifts=seq(0,3,0.5) # how much to move the basic BAF function along the x axis
            score=rep(Inf,length(shifts)) # goodness of fit
            h=rep(Inf,length(shifts))
            if (is.na(shift)) {    ## This is the first (biggest/outer) cluster, and shift=NA (set above)
                for (s in 1:length(shifts)) try( { # try different starting positions for the function
                    x=5:20
                    md=lm(tbaf-0.5 ~ bm(tval,shifts[s],curve)+0) # fit a model to get "h" which scales the basic function to fit data
                    h[s]=md$coefficients[1][[1]]
                    y=h[s]*bm(x,shifts[s],curve)+0.5
                    # if (plot) lines(x=x,y=y,col='green')
                    score[s]=median(abs(md$residuals))
                    if (sum(y>1 | y<0, na.rm=T) > 0) score[s]=Inf # This is to stop function from running over 1/under 0.
                }, silent=T)
                best=order(score)[1]
                shift=shifts[best] # retrieve and keep best shift for the other clusters of this NSP
                h=h[[best]][[1]] #retrieve the best fitting "h"
                if (plot) lines(x=x,y=h*bm(x,shift,curve)+0.5,col=colLine,lwd=lwdLine)
                ## save cluster data.
                
                if (cl==1) {
                    aa.h=h
                    aa.median=median(tval)
                } else {
                    bb.h=h
                    bb.median=median(tval)
                }
                
            } else {
                x=5:20
                md=lm(tbaf-0.5 ~ bm(tval,shift,curve)+0)
                h=md$coefficients[1][[1]]
                if (plot) lines(x=x,y=h*bm(x,shift,curve)+0.5,col=colLine,lwd=lwdLine)
                
                ## save cluster data.
                if (cl==1) { # first cluster always AA
                    aa.h=h
                    aa.median=median(tval)
                } else if (cl==2 & length(order)==3) { # 2nd cluster AB if 3 clusters total
                    ab.h=h
                    ab.median=median(tval)
                } else {
                    bb.h=h
                    bb.median=median(tval)
                }
            }
        }
        dev.off()
        
    } else { # done with "if OK"
        #Tillagt september -14. Ordnar en robust lösning för SNPs av sämre kvalite.
        km=NULL
        homCluster=NULL
        try( { # Using clusters again, try extracting the most distant cluster that is also well populated.
            km2=kmeans(baf,2)
            km3=kmeans(baf,3)
            if (km2$totss/length(km2$cluster) < km3$totss/length(km3$cluster)) km=km2 else km=km3
            dists=abs(km$centers-0.5)
            dists[km$size<300]=0
            homCluster=which(km$cluster==which.max(dists))
            }, silent=T)
        
        if (is.null(homCluster)) { # If for some reason that would not work
            med=median(abs(baf-0.5))
            med=max(med,.2)
            upper=which(abs(baf-(0.5+med))<0.1)
            lower=which(abs(baf-(0.5-med))<0.1)
            if (length(upper)>length(lower)) homCluster=upper else homCluster=lower
        }
        
        ## Now a simplified version of the assignment of h and shift above
        shift=NA # BAF function position on x axis
        tbaf=baf[homCluster] # baf of this cluster
        tval=val[homCluster] # values of this cluster
            
        ## optimize BAF function height and shift so that tbaf ~ 0.5+h*bm(tval+shift)
        shifts=seq(0,3,0.5) # how much to move the basic BAF function along the x axis
        score=rep(Inf,length(shifts)) # goodness of fit
        h=rep(Inf,length(shifts))
        for (s in 1:length(shifts)) try( { # try different starting positions for the function
            x=5:20
            md=lm(tbaf-0.5 ~ bm(tval,shifts[s],curve)+0) # fit a model to get "h" which scales the basic function to fit data
            h[s]=md$coefficients[1][[1]]
            y=h[s]*bm(x,shifts[s],curve)+0.5
            # if (plot) lines(x=x,y=y,col='green')
            score[s]=median(abs(md$residuals))
            if (sum(y>1 | y<0, na.rm=T) > 0) score[s]=Inf # This is to stop function from running over 1/under 0.
        }, silent=T)
        best=order(score)[1]
        shift=shifts[best] # retrieve and keep best shift for the other clusters of this SNP
        h=h[[best]][[1]] #retrieve the best fitting "h"
        # if (plot) lines(x=x,y=h*bm(x,shift,curve)+0.5,col='red')
        if (plot) lines(x=x,y=h*bm(x,shift,curve)+0.5,col=colLine,lwd=lwdLine)
        if (plot) lines(x=x,y=-h*bm(x,shift,curve)+0.5,col=colLine,lwd=lwdLine)
        if (plot) lines(x=x,y=0*h*bm(x,shift,curve)+0.5,col=colLine,lwd=lwdLine)
        if (plot) print('bad')
        ## save cluster data.
        if (mean(tbaf)<0.5) {
            aa.h=h
            aa.median=median(tval)
            bb.h=-h
            bb.median=NA
        } else {
            bb.h=h
            bb.median=median(tval)
            aa.h=-h
            aa.median=NA
        }
        ab.h=0
        ab.median=NA
    } # done with "else" (lower-qual SNPs)
        

    browser()
    plot<-F

    
    #browser()
    
    #}# temporary end of SNP loop
    
    ### Add data equivalent to that of the copy probes (SNPs may also be BAF normalized though).
    ## Note that all SNPs get this, while many (!good) were not previously used for BAF


    col='black'; if (!is.null(km)) col=km$cluster
    if (plot) plot(sampleData[G,type[i]],snpsums[i,G],col=col)
    
    # If autosomal or MT, make one line
    if (!tX[i] & !tY[i]) {
        line=line(sampleData[G,type[i]],snpsums[i,G])
        #browser()
        k[i]=line$coefficients[2] 
        m[i]=line$coefficients[1]
        if (plot) lines(x=5:15,y=k[i]*(5:15)+m[i])
        meanResidual[i]=mean(abs(line$residuals))
    }
    
    # If X chrom, make separate lines for diploid (female) and haploid (male) samples
    if (tX[i]) {
        #females
        line=line(sampleData[!male & G,type[i]],snpsums[i,!male & G])
        k[i]=line$coefficients[2] 
        m[i]=line$coefficients[1]
        if (plot) lines(x=5:15,y=k[i]*(5:15)+m[i])
        #hapX[i]=median(k[i]*sampleData[male,type[i]]+m[i]-snpsums[i,male]) #how much drop caused by haploid X
        meanResidual[i]=mean(abs(line$residuals))
        #males
        line=line(sampleData[male & G,type[i]],snpsums[i,male & G])
        haploid.k[i]=line$coefficients[2] 
        haploid.m[i]=line$coefficients[1]
        if (plot) lines(x=5:15,y=haploid.k[i]*(5:15)+haploid.m[i])
    }
    # If Y chrom, make lines for haploid (male) and absent (female) samples
    if (tY[i]) {
        #male
        line=line(sampleData[male & G,type[i]],snpsums[i,male & G])
        haploid.k[i]=line$coefficients[2] 
        haploid.m[i]=line$coefficients[1]
        if (plot) lines(x=5:15,y=haploid.k[i]*(5:15)+haploid.m[i])
        noY[i]=median(haploid.k[i]*sampleData[!male,type[i]]+haploid.m[i]-snpsums[i,!male]) #how much drop caused by Y absence
        meanResidual[i]=mean(abs(line$residuals))
        #female
        line=line(sampleData[!male & G,type[i]],snpsums[i,!male & G])
        absent.k[i]=line$coefficients[2] 
        absent.m[i]=line$coefficients[1]
        if (plot) lines(x=5:15,y=absent.k[i]*(5:15)+absent.m[i])
    }  
    # browser()
}### End of the big loop

#################### To here
# load the result from getSNPfunction
snpClusters <- result
    

# snpClusters$good=good
# snpClusters$shift=shift
# snpClusters$bb.h=bb.h
# snpClusters$bb.median=bb.median
# snpClusters$ab.h=ab.h
# snpClusters$ab.median=ab.median
# snpClusters$aa.h=aa.h
# snpClusters$aa.median=aa.median
# snpClusters$k=k
# snpClusters$m=m
# snpClusters$haploid.k=haploid.k
# snpClusters$haploid.m=haploid.m
# snpClusters$absent.k=absent.k
# snpClusters$absent.m=absent.m
# snpClusters$meanResidual=meanResidual
# 
# 
# summary(meanResidual)
# hist(meanResidual,seq(0,1.4,.01)) ## This may eventually be used to remove (technically) bad snp log-r values from the ref data.

##### fill in k and m for chromosomes Y using that of haploid (males)
snpClusters$k[tY]=snpClusters$haploid.k[tY]
snpClusters$k2[tY]=snpClusters$haploid.k2[tY]
snpClusters$m[tY]=snpClusters$haploid.m[tY]

### chrM is not an issue with the cn.probes as all markers on chrM are SNPs (SNP6).

colnames(snpClusters[1,c("medianBaf....median.baf..na.rm...T.", "medianVal....median.val..na.rm...T.")]) <- c('medianBaf','medianVal')
colnames(snpClusters)[18:19] <-  c('medianBaf','medianVal')
### Done with snpClusters, store for later use.
save(snpClusters,file=paste("/media/safe/bjorn/rawcopyData/SNP6.snpClusters.",format(Sys.time(), "%F_%H_%M_%S"),".Rdata",sep=''))
save(snpClusters,file=paste("/media/safe/bjorn/rawcopyData/CytoScan.snpClusters.",format(Sys.time(), "%F_%H_%M_%S"),".Rdata",sep=''))



# ____            _               
#|  _ \ _ __ ___ | |__   ___  ___ 
#| |_) | '__/ _ \| '_ \ / _ \/ __|
#|  __/| | | (_) | |_) |  __/\__ \
#|_|   |_|  \___/|_.__/ \___||___/
#                                 
#Probes
################## Process PROBEs into clusters and stats.

load('/media/safe/bjorn/readCELfileReferences/probes.Rdata')

# load('/media/safe/bjorn/git/rawcopy/rawcopyReference/data/CytoScanHD_probes_2014-08-09_12:29:46.Rdata')
library(rgl)
load('/media/safe/bjorn/git/rawcopy/rawcopyReference/data/CytoScanHD_probes_2014-08-09_12_29_46.Rdata')
# load('data/CytoScanHD_probes_2014-08-09_12_29_46.Rdata')

probeData=probes[,1:2]
probes=probes[,-(1:2)]
gc()

### Load snp annotation and match it with probe data
load('/media/safe/bjorn/git/rawcopy/rawcopyReference/data/pannot.Rdata')
# load('data/CytoScanHD_pannot_2014-06-25_14:54:29.Rdata')
load('/media/safe/bjorn/git/rawcopy/rawcopyReference/data/CytoScanHD_pannot_2014-06-25_14_54_29.Rdata')
pannot=merge(probeData,pannot,by=1,all.x=T,all.y=F)
pannot=pannot[order(pannot$ix),]
gc()

### QC!!!
sampleData=data.frame(sample=colnames(probes)) 
sampleData$probe.nsp=NA
sampleData$probe.sty=NA
sampleData$probe.both=NA
sampleData$probe.X=NA
sampleData$probe.Y=NA
sampleData$probe.MT=NA
sampleData$probe.MAPD=NA # nsp-only probes

## QCs
for (s in 1:nrow(sampleData)) {
    sampleData$probe.nsp[s]=round((median(probes[!pannot$sty & pannot$Chromosome=='1',s])),2)
    sampleData$probe.sty[s]=round((median(probes[!pannot$nsp & pannot$Chromosome=='1',s])),2)
    sampleData$probe.both[s]=round((median(probes[pannot$nsp & pannot$sty & pannot$Chromosome=='1',s])),2)
    sampleData$probe.X[s]=round((median(probes[
        pannot$nsp & pannot$Chromosome=='X' & pannot$ChrX.pseudo.autosomal.region.1!=1 & pannot$ChrX.pseudo.autosomal.region.2!=1,s]) - 
                                     median(probes[pannot$nsp & pannot$Chromosome=='1',s])),2)
    sampleData$probe.Y[s]=round((median(probes[pannot$nsp & pannot$Chromosome=='Y',s]) - 
                                     median(probes[pannot$nsp & pannot$Chromosome=='1',s])),2)
    sampleData$probe.MT[s]=round((median(probes[pannot$Chromosome=='MT',s]) - 
                                      median(probes[pannot$Chromosome=='1',s])),2)
    ix=which(pannot$Chromosome=='1')
    ix=ix[order(pannot$Chromosome.Start[ix])]
    sampleData$probe.MAPD[s]=round(median(abs(diff(probes[ix,s]))),2)
    print(paste(s,'/',nrow(sampleData),sep=''))
    print(sampleData[s,])
}

# save(sampleData,file='data/SNP6.sampleDataP.Rdata')
# save.image(file='data/cytoscan.sampleDataP.Rdata')
save(sampleData,file=(paste('/media/safe/bjorn/git/rawcopy/rawcopyReference/data/CytoScanHD_sampleDataP',format(Sys.time(), "%F_%H_%M_%S"),'.Rdata',sep='')))
save(sampleData,file=(paste('/media/safe/bjorn/git/rawcopy/rawcopyReference/data/SNP6_sampleDataP',format(Sys.time(), "%F_%H_%M_%S"),'.Rdata',sep='')))
## Investigate probe intensities
par(mfrow=c(2,3),mar=c(4,4,1,1))
plot(sampleData$probe.nsp,sampleData$probe.MAPD,pch=20,col='#00000040',xlab='NSP probe hybridization',ylab='NSP probe MAPD')
plot(sampleData$probe.nsp,sampleData$probe.X,pch=20,col='#00000040',xlab='NSP probe hybridization',ylab='NSP X')
plot(sampleData$probe.nsp,sampleData$probe.Y,pch=20,col='#00000040',xlab='NSP probe hybridization',ylab='NSP Y')
plot(sampleData$probe.MAPD,sampleData$probe.X,pch=20,col='#00000040',xlab='NSP probe MAPD',ylab='NSP X')
plot(sampleData$probe.MAPD,sampleData$probe.Y,pch=20,col='#00000040',xlab='NSP probe MAPD',ylab='NSP Y')
dev.off()

## Remove samples unsuitable for probe reference data


#------------
#Cutoffs på SNP6
# sampleData[sampleData$probe.nsp<8,]
# 
# bad=sampleData$probe.both<9.5 |
#     sampleData$probe.X < -0.7 |
#     ((sampleData$probe.X > -0.45) & (sampleData$probe.X < -0.1))
#genderCutOff <- -0.3
#Cutoffs på CytoScan

load('/media/safe/bjorn/git/rawcopy/rawcopyReference/data/CytoScanHD_sampleDataP2014-11-26_11_46_18.Rdata')
#CytoScanHD cutoffs
sampleData[sampleData$probe.nsp<8,]
bad=is.na(sampleData$probe.MAPD) | sampleData$probe.MAPD > 0.8 | (sampleData$probe.X > -0.7 & sampleData$probe.X < -0.6) | sampleData$probe.nsp < 9.5 |
    sampleData$probe.MAPD + 0.075*sampleData$probe.nsp > 1.5125
G <- !bad
genderCutOff <- -0.7
#------------

#SNP6 cutoffs
# sampleData[sampleData$probe.nsp<8,]
bad=is.na(sampleData$probe.MAPD) | sampleData$probe.MAPD > 0.9 | (sampleData$probe.X > -0.7 & sampleData$probe.X < -0.6) | sampleData$probe.nsp < 9 | 
    sampleData$probe.nsp > 12
G <- !bad
genderCutOff <- -0.3

#------------


type=rep(2,nrow(probes))
type[pannot$nsp & pannot$sty]=4


#### Process each probe and store reference data
probeClusters=probeData

male=sampleData$probe.X < genderCutOff # to be treated as male samples
x=pannot$Chromosome=='X' & pannot$ChrX.pseudo.autosomal.region.1!=1 & pannot$ChrX.pseudo.autosomal.region.2!=1 # to be treated as X probes
y=pannot$Chromosome=='Y' # to be treated as Y probes

tY <- y
tX <- x

k <- m <- haploid.k <- haploid.m <- absent.k <- absent.m <- meanResidual <- rep(NA,nrow(probeClusters))
k2 <- haploid.k2 <- absent.k2 <- peaks <-  k 

plot=T
if(plot) par(mfrow=c(2,1))
t=1:1000
col='#000000'
library(MASS)
library(rgl)
# for (i in c(721,1e3,2e3,3e3,which(tY)[1:3],which(tX)[1:3])) {
# for (i in seq(5201,nrow(probeData),100)) {
# b <- 124
# for (i in 124) {
# for (i in c(721,1e3,2e3,3e3,which(tY)[1:3],which(tX)[1:3])) {
for (i in 1:nrow(probeData)) {
    print(i)
    if (plot){ 
        plot(sampleData[,type[i]],as.numeric(probes[i,]),col=col)
        # plot3d(sampleData[G,type[i]],sampleData$probe.MAPD[G],probes[i,G],xlab='',ylab='',zlab='',col='black',size=8,aspect=T)
        plot3d(sampleData[G,type[i]],sampleData$probe.MAPD[G],probes[i,G],xlab='',ylab='',zlab='',col='black',size=1,aspect=T)
        # plot3d(sampleData[G,type[i]][1],sampleData$probe.MAPD[G][1],probes[i,G][1],xlab='',ylab='',zlab='',col='black',size=8,aspect=T)
        values <- data.frame(sampleData[G,type[i]],sampleData$probe.MAPD[G],as.numeric(probes[i,G]))
        for(q in 1:nrow(values)) {
            print(q)
            points3d(values[q,],size=(8/values[q,2]^2)/2,alpha=0.5)
        }

        # plot3d(sampleData[G,type[i]],sampleData$probe.MAPD[G],probes[i,G],xlab='Hybridization',ylab='MAPD',zlab='Value',col='black',size=8,aspect=T)
        # plot3d(sampleData[G,type[i]][1:2],sampleData$probe.MAPD[G][1:2],probes[i,G][1:2],xlab='Hybridization',ylab='MAPD',zlab='Value',col='black',size=5)
        grid3d(c('x-','Y+','z'))
    }
    if (!x[i] & !y[i]) {

        rlm <- rlm(z~x+y,data=data.frame(x=sampleData[G,type[i]],y= sampleData$probe.MAPD[G]  ,z=as.numeric(probes[i,G])))
        k[i] <- rlm$coefficients[2]
        k2[i] <- rlm$coefficients[3]
        m[i]<- rlm$coefficients[1]
        d=density(rlm$residuals)
        peak=d$x[which.max(d$y)]
        m[i]=m[i]+peak

        resi=rlm$residuals-peak
        resi=resi[sign(resi)==sign(peak)]
        meanResidual[i]=mean(resi,trim=0.05)

        if(plot) {


            # plot3d(x=1:4,y=c(1:3,7),z=1:4,size=10)
            # segments3d(c(1,4),c(1,7),c(1,4))
            # segments3d(x=rep(x[1],2),y=rep(y[1],2),z=c(z[1],z2[1]))
            # segments3d(x=rep(x,2),y=rep(y,2),z=c(z,z2))






            z2 <- rlm$coefficients[2] * sampleData[G,type[i]] + rlm$coefficients[3] * sampleData$probe.MAPD[G] + rlm$coefficients[1] + peak

            #             segments3d(c(x,x),c(y,y),c(z,z2),col='red',lwd=2)
            #             segments3d(c(x[1:10],x[1:10]),c(y[1:10],y[1:10]),c(z[1:10],z2[1:10]),col='red',lwd=2)
            # 
            a <- k[i]
            b <- k2[i]
            c <- m[i]
            planes3d(a=a,b=b,c=-1,d=c,alpha=.5,texture='~/boom.png',textype='luminance')


            a <- sampleData[G,type[i]]
            b <- sampleData$probe.MAPD[G]
            c <- as.numeric(probes[i,G])

            a <- rep(a,each=2)
            b <- rep(b,each=2)
            C <- rep(c,each=2)
            C[seq(2,length(C),by=2)] <- z2 
            c <- C
            segments3d(a,b,c,col='grey',lwd=1)
            points3d(sampleData[G,type[i]],sampleData$probe.MAPD[G],as.numeric(probes[i,G]),col="#000000",size=(16 * sampleData$probe.MAPD[G]))


            #             surface3d(x=x,y=y,z=(z))
            #             planes3d(a=x,b=y,c=-1,d=z,alpha=.3)
            #             surface3d(unique(Agriculture),unique(Education),pp,alpha=0.3,front="line")
            # 
            #             points3d(x=x[1],y= y[1],z=z2[1],col='#0000FF',size=10)

            # points3d(x=sampleData[G,type[i]],y= sampleData$probe.MAPD[G],
            #          z=rlm$coefficients[2] * sampleData[G,type[i]] + rlm$coefficients[3] * sampleData$probe.MAPD[G] + 
            #          rlm$coefficients[1] + peak,col='#0000FF')
        }

    }
    
            browser()
    # If X chrom, make separate lines for diploid (female) and haploid (male) samples
    if (x[i]) {
        #females
        rlm <- rlm(z~x+y,data=data.frame(x=sampleData[!male & G,type[i]],y= sampleData$probe.MAPD[!male & G]  ,z=as.numeric(probes[i,!male & G])))
        k[i] <- rlm$coefficients[2]
        k2[i] <- rlm$coefficients[3]
        m[i] <- rlm$coefficients[1]
        d=density(rlm$residuals)
        peak=d$x[which.max(d$y)]
        m[i]=m[i]+peak

        resi=rlm$residuals-peak
        resi=resi[sign(resi)==sign(peak)]
        meanResidual[i]=mean(resi,trim=0.05)

        if(plot) {
            points3d(x=sampleData[!male & G,type[i]],y= sampleData$probe.MAPD[!male & G],
                     z=rlm$coefficients[2] * sampleData[!male & G,type[i]] + rlm$coefficients[3] * sampleData$probe.MAPD[!male & G] + 
                     rlm$coefficients[1] + peak,col='#FF00FF')
        }
        #males
        rlm <- rlm(z~x+y,data=data.frame(x=sampleData[male & G,type[i]],y= sampleData$probe.MAPD[male & G]  ,z=as.numeric(probes[i,male & G])))
        haploid.k[i] <- rlm$coefficients[2]
        haploid.k2[i] <- rlm$coefficients[3]
        haploid.m[i] <- rlm$coefficients[1]
        d=density(rlm$residuals)
        h.peak=d$x[which.max(d$y)]
        haploid.m[i]=haploid.m[i]+h.peak

        if(plot) {
            points3d(x=sampleData[male & G,type[i]],y= sampleData$probe.MAPD[male & G],
                     z=rlm$coefficients[2] * sampleData[male & G,type[i]] + rlm$coefficients[3] * sampleData$probe.MAPD[male & G] + 
                     rlm$coefficients[1] + peak,col='#FF00FF')
        }
    }
    # If Y chrom, make lines for haploid (male) and absent (female) samples
    if (y[i]) {
        #male
        rlm <- rlm(z~x+y,data=data.frame(x=sampleData[male & G,type[i]],y= sampleData$probe.MAPD[male & G]  ,z=as.numeric(probes[i,male & G])))
        haploid.k[i] <- rlm$coefficients[2]
        haploid.k2[i] <- rlm$coefficients[3]
        haploid.m[i] <- rlm$coefficients[1]
        d=density(rlm$residuals)
        peak=d$x[which.max(d$y)]
        haploid.m[i]=haploid.m[i]+peak

        resi=rlm$residuals-peak
        resi=resi[sign(resi)==sign(peak)]
        meanResidual[i]=mean(resi,trim=0.05)

        if(plot) {
            points3d(x=sampleData[male & G,type[i]],y= sampleData$probe.MAPD[male & G],
                     z=rlm$coefficients[2] * sampleData[male & G,type[i]] + rlm$coefficients[3] * sampleData$probe.MAPD[male & G] + 
                     rlm$coefficients[1] + peak,col='#FF00FF')
        }
        #female
        rlm <- rlm(z~x+y,data=data.frame(x=sampleData[!male & G,type[i]],y= sampleData$probe.MAPD[!male & G]  ,z=as.numeric(probes[i,!male & G])))
        absent.k[i] <- rlm$coefficients[2]
        absent.k2[i] <- rlm$coefficients[3]
        absent.m[i] <- rlm$coefficients[1]
        d=density(rlm$residuals)
        a.peak=d$x[which.max(d$y)]
        absent.m[i]=absent.m[i]+a.peak
        if(plot) {
            points3d(x=sampleData[!male & G,type[i]],y= sampleData$probe.MAPD[!male & G],
                     z=rlm$coefficients[2] * sampleData[!male & G,type[i]] + rlm$coefficients[3] * sampleData$probe.MAPD[!male & G] + 
                     rlm$coefficients[1] + peak,col='#FF00FF')
        }
    }   
    if (trunc(i/1e3)==i/1e3) cat(i,'\n')  
    if(plot) {
        a <- rlm$residuals
        plot(density(a))
    }
    peaks[i] <- peak
}

probeClusters$k=k
probeClusters$k2=k2
probeClusters$m=m
probeClusters$haploid.k=haploid.k
probeClusters$haploid.k2=haploid.k2
probeClusters$haploid.m=haploid.m
probeClusters$absent.k=absent.k
probeClusters$absent.k2=absent.k2
probeClusters$absent.m=absent.m
probeClusters$meanResidual=meanResidual
probeClusters$peak=peaks

#ix=1:10000
#plot(k[ix],m[ix],pch=20,col='black') ### k and m are strongly correlated of course
#points(haploid.k,haploid.m,pch=20,col='blue')
#points(absent.k,absent.m,pch=20,col='red')

summary(meanResidual)
hist(meanResidual,seq(0,1.4,.01)) ## This may eventually be used to remove (technically) bad probes from the ref data.

##### fill in k and m for chromosomes Y using that of haploid (males)
probeClusters$k[y]=probeClusters$haploid.k[y]
probeClusters$k2[y]=probeClusters$haploid.k2[y]
probeClusters$m[y]=probeClusters$haploid.m[y]

### chrM is not an issue with the cn.probes as all markers on chrM are SNPs (SNP6).

### Done with probeClusters, store for later use.
save(probeClusters,file=paste("/media/safe/bjorn/git/rawcopy/rawcopyReference/data/SNP6.probeClusters.",format(Sys.time(), "%F_%H_%M_%S"),".Rdata",sep=''))
save(probeClusters,file=paste("/media/safe/bjorn/git/rawcopy/rawcopyReference/data/CytoScanHD.probeClusters.",format(Sys.time(), "%F_%H_%M_%S"),".Rdata",sep=''))


