1. processReferenceCELfiles.r
* Create rawcopy reference, works with CytoScanHD and SNP 6.0
* Collect snps and probe intensities from your reference samples
Infiles:
Your set of reference CEL-files
CytoScanHD_Array.cdf
GenomeWideSNP_6.cdf

Outfiles:
CytoScanHD_snps.Rdata
CytoScanHD_probes.Rdata

2. makeReferenceData.r
* Create custom functions for each snp and probe.
Infiles: 
CytoScanHD_annot.Rdata
CytoScanHD_snps.Rdata
CytoScanHD_probes.Rdata

Outfiles:
CytoScanHD_snpClusters.rda
CytoScanHD_probeClusters.rda

3. mds6dimSNP6RefAndSample.R
*Rawcopy has to be run beforehand with MDS=F.
Infiles: rawcopy.Rdata from all samples.

Outfiles:
CytoScanHD_mds.rda
