
### Läs annoteringsfiler och modda med fragment- och GC-info.

setwd('/media/safe/bjorn/git/rawcopy')


# ____                _   ____  _   _ ____                            _        _   _             
#|  _ \ ___  __ _  __| | / ___|| \ | |  _ \    __ _ _ __  _ __   ___ | |_ __ _| |_(_) ___  _ __  
#| |_) / _ \/ _` |/ _` | \___ \|  \| | |_) |  / _` | '_ \| '_ \ / _ \| __/ _` | __| |/ _ \| '_ \ 
#|  _ <  __/ (_| | (_| |  ___) | |\  |  __/  | (_| | | | | | | | (_) | || (_| | |_| | (_) | | | |
#|_| \_\___|\__,_|\__,_| |____/|_| \_|_|      \__,_|_| |_|_| |_|\___/ \__\__,_|\__|_|\___/|_| |_|
#                                                                                                
#Read SNP annotation

#---
#Test for na34
source('functions.r')
t=0; for (i in 1:100) t[i]=ncol(read.csv('data/GenomeWideSNP_6.na34.annot.csv',header=F,nrow=1,skip=i))
annot=read.csv('data/GenomeWideSNP_6.na34.annot.csv',skip=which(t==max(t))[1], nrow=10)

t=0; for (i in 1:100) t[i]=ncol(read.csv('data/CytoScanHD_Array.na32.3.annot.csv',header=F,nrow=1,skip=i))
annot=read.csv('data/CytoScanHD_Array.na32.3.annot.csv',skip=which(t==max(t))[1], nrow=12)

#Old na32
# t=0; for (i in 1:100) t[i]=ncol(read.csv('data/GenomeWideSNP_6.na32.annot.csv',header=F,nrow=1,skip=i))
# annot=read.csv('data/GenomeWideSNP_6.na32.annot.csv',skip=which(t==max(t))[1], nrow=10)
#---

names=names(annot)
selected=c("Probe.Set.ID", "Chromosome", "ChrX.pseudo.autosomal.region.1", 
           "Allele.A",'Allele.B', "Fragment.Enzyme.Type.Length.Start.Stop", "ChrX.pseudo.autosomal.region.2", 
           "X..GC")
colC=rep('NULL',length(names))
colC[names %in% selected]='character'
colC[names=="Physical.Position"]=NA

# annot=read.csv('data/GenomeWideSNP_6.na34.annot.csv',skip=which(t==max(t))[1], colClasses=colC)
annot=read.csv('data/CytoScanHD_Array.na32.3.annot.csv',skip=which(t==max(t))[1], colClasses=colC)

### SNP fragments
snpfrags=data.frame(Name=annot$Probe.Set.ID, Chr=annot$Chromosome)
nsp.start <- nsp.end <- nsp.length <- sty.start <- sty.end <- sty.length <- rep(NA,nrow(snpfrags))
splits=strsplit(annot$Fragment.Enzyme.Type.Length.Start.Stop, ' /// ')
for (i in 1:nrow(annot)) {
    # cat(i,'\n')
    for (j in 1:length(splits[[i]])) {
        temp=strsplit(splits[[i]][j],' // ')[[1]]
        if (temp[1]=='NspI') {
            nsp.start[i]=as.integer(temp[4])
            nsp.end[i]=as.integer(temp[5])
            nsp.length[i]=as.integer(temp[3])
        } else if (temp[1]=='StyI') {
            sty.start[i]=as.integer(temp[4])
            sty.end[i]=as.integer(temp[5])
            sty.length[i]=as.integer(temp[3])
        }
    }
}
snpfrags=cbind(snpfrags,nsp.start,nsp.end,nsp.length,sty.start,sty.end,sty.length)

snpfrags$nsp=!is.na(nsp.start); snpfrags$nsp[snpfrags$nsp]=snpfrags$nsp.length[snpfrags$nsp]<2000 
snpfrags$sty=!is.na(sty.start); snpfrags$sty[snpfrags$sty]=snpfrags$sty.length[snpfrags$sty]<2000 
snpfrags$nsp[!snpfrags$nsp & !snpfrags$sty]=T


# both=nsp.length<1100 & sty.length<1100
# both[is.na(both)]=F
# 
# shortest=apply(cbind(nsp.length,sty.length),1,function(x) {
#                order(x)[1]
#            })
# 
# snpfrags$nsp=!is.na(nsp.start) & (shortest == 1 | both)
# snpfrags$sty=!is.na(sty.start) & (shortest == 2 | both)



# ix <- snpfrags$Chr %in% c(1:22,'X','Y','MT')
# snpfrags <- snpfrags[ix,]
# annot <- annot[ix,]
sum(!snpfrags$nsp) #160881
sum(!snpfrags$sty) #245663
sum(snpfrags$nsp&snpfrags$sty) #530452

## Add GC content to snpfrags
snpfrags$sty.gc <- snpfrags$nsp.gc <- NA
for (c in as.character(unique(snpfrags$Chr))) {
    cat(c,'\n')
    if( file.exists(paste('data/GC_hg19/chr',c,'.gc5.Rdata',sep='')) ){
        load(paste('data/GC_hg19/chr',c,'.gc5.Rdata',sep=''))
    } else next()
    ix=which(snpfrags$Chr==c & snpfrags$nsp)
    snpfrags$nsp.gc[ix]=regionMeans(snpfrags$nsp.start[ix],snpfrags$nsp.end[ix],gc5$pos,gc5$gc)
    ix=which(snpfrags$Chr==c & snpfrags$sty)
    if(length(ix) > 0 ) {
        snpfrags$sty.gc[ix]=regionMeans(snpfrags$sty.start[ix],snpfrags$sty.end[ix],gc5$pos,gc5$gc)
    }
    rm(gc5)
}

annot=cbind(annot,snpfrags[,-1])

## Adda förenklat GC till annot:
gc=rep(NA,nrow(annot)) 
gc[!annot$sty] <- annot$nsp.gc[!annot$sty]
gc[!annot$nsp] <- annot$sty.gc[!annot$nsp]
gc[is.na(gc)] <- (annot$nsp.gc[is.na(gc)]+annot$sty.gc[is.na(gc)])/2
annot$gc=gc

save(annot,file='annot.Rdata')
save(annot,file=paste("data/CytoScanHD_annot_",format(Sys.time(), "%F_%T"),".Rdata",sep=''))

# ____                _   ____            _                                    _        _   _             
#|  _ \ ___  __ _  __| | |  _ \ _ __ ___ | |__   ___    __ _ _ __  _ __   ___ | |_ __ _| |_(_) ___  _ __  
#| |_) / _ \/ _` |/ _` | | |_) | '__/ _ \| '_ \ / _ \  / _` | '_ \| '_ \ / _ \| __/ _` | __| |/ _ \| '_ \ 
#|  _ <  __/ (_| | (_| | |  __/| | | (_) | |_) |  __/ | (_| | | | | | | | (_) | || (_| | |_| | (_) | | | |
#|_| \_\___|\__,_|\__,_| |_|   |_|  \___/|_.__/ \___|  \__,_|_| |_|_| |_|\___/ \__\__,_|\__|_|\___/|_| |_|
#                                                                                                         
#Read Probe annotation

#---
#Test for na34
source('functions.r')
t=0; for (i in 1:100) t[i]=ncol(read.csv('data/GenomeWideSNP_6.cn.na34.annot.csv',header=F,nrow=1,skip=i))
pannot=read.csv('data/GenomeWideSNP_6.cn.na34.annot.csv',skip=which(t==max(t))[1], nrow=10)

t=0; for (i in 1:100) t[i]=ncol(read.csv('data/CytoScanHD_Array.cn.na32.3.annot.csv',header=F,nrow=1,skip=i))
pannot=read.csv('data/CytoScanHD_Array.cn.na32.3.annot.csv',skip=which(t==max(t))[1], nrow=10)

#Old na32
# t=0; for (i in 1:100) t[i]=ncol(read.csv('data/GenomeWideSNP_6.cn.na32.annot.csv',header=F,nrow=1,skip=i))
# pannot=read.csv('data/GenomeWideSNP_6.cn.na32.annot.csv',skip=which(t==max(t))[1], nrow=10)
#---

names=names(pannot)
colC=rep('NULL',length(names))

ix <- names %in% c("Probe.Set.ID", "Chromosome", "ChrX.pseudo.autosomal.region.1",
             "Fragment.Enzyme.Type.Length.Start.Stop", 
             "ChrX.pseudo.autosomal.region.2", "SNP.Interference", 
             "X..GC", "In.Final.List")

colC[ix]='character' 
colC[3:4]=NA
pannot=read.csv('data/CytoScanHD_Array.cn.na32.3.annot.csv',skip=which(t==max(t))[1], colClasses=colC)
# pannot=read.csv('data/GenomeWideSNP_6.cn.na34.annot.csv',skip=which(t==max(t))[1], colClasses=colC)

# ix <- pannot$Chromosome %in% c(1:22,'X','Y','MT')
# pannot <- pannot[ix,]

### Probe fragments
probefrags=data.frame(Name=pannot$Probe.Set.ID, Chr=pannot$Chromosome)
nsp.start <- nsp.end <- nsp.length <- sty.start <- sty.end <- sty.length <- rep(NA,nrow(probefrags))
splits=strsplit(pannot$Fragment.Enzyme.Type.Length.Start.Stop, ' /// ')
for (i in 1:nrow(pannot)) {
    cat(i,'\n')
    for (j in 1:length(splits[[i]])) {
        temp=strsplit(splits[[i]][j],' // ')[[1]]
        if (temp[1]=='NspI') {
            nsp.start[i]=as.integer(temp[4])
            nsp.end[i]=as.integer(temp[5])
            nsp.length[i]=as.integer(temp[3])
        } else if (temp[1]=='StyI') {
            sty.start[i]=as.integer(temp[4])
            sty.end[i]=as.integer(temp[5])
            sty.length[i]=as.integer(temp[3])
        }
    }
}
probefrags=cbind(probefrags,nsp.start,nsp.end,nsp.length,sty.start,sty.end,sty.length)

both=nsp.length<1100 & sty.length<1100
both[is.na(both)]=F
shortest=apply(cbind(nsp.length,sty.length),1,function(x) {
               order(x)[1]
           })

probefrags$nsp=!is.na(nsp.start) & (shortest == 1 | both)
probefrags$sty=!is.na(sty.start) & (shortest == 2 | both)

sum(!probefrags$nsp) #221
sum(!probefrags$sty) #431055
sum(probefrags$nsp&probefrags$sty) #514766

## Add GC content to probe frags
probefrags$sty.gc <- probefrags$nsp.gc <- NA

for (c in unique(probefrags$Chr)) try( {
    cat(c,'\n')

    if( file.exists(paste('data/GC_hg19/chr',c,'.gc5.Rdata',sep='')) ){
        load(paste('data/GC_hg19/chr',c,'.gc5.Rdata',sep=''))
    }
    # load(paste('data/GC_hg19/chr',c,'.gc5.Rdata',sep=''))
    ix=which(probefrags$Chr==c & probefrags$nsp)
    probefrags$nsp.gc[ix]=regionMeans(probefrags$nsp.start[ix],probefrags$nsp.end[ix],gc5$pos,gc5$gc)
    ix=which(probefrags$Chr==c & probefrags$sty)
    probefrags$sty.gc[ix]=regionMeans(probefrags$sty.start[ix],probefrags$sty.end[ix],gc5$pos,gc5$gc)
    rm(gc5)
}, silent=T)

pannot=cbind(pannot,probefrags[,-1])

## Fixa fraglängd i pannot: ### Ej testat om detta sabbar Cytoscan!!
p=as.numeric(as.character(pannot$Chromosome.Start))
s=pannot$sty.start
e=pannot$sty.end
na=is.na(pannot$sty.length)
for (c in unique(pannot$Chromosome)) {
    ix=pannot$Chromosome==c
    ix2=annot$Chromosome==c
    sty=unique(c(pannot$sty.start[ix],pannot$sty.end[ix],annot$sty.start[ix2],annot$sty.end[ix2]))
    sty=sty[!is.na(sty)]
    ix3=which(ix&na)
    for (i in ix3) {
        s[i]=max(sty[sty<p[i]],na.rm=T)
        e[i]=min(sty[sty>p[i]],na.rm=T)
    }
}
pannot$sty.start=s
pannot$sty.end=e
pannot$sty.length=e-s

s=pannot$nsp.start
e=pannot$nsp.end
na=is.na(pannot$nsp.length)
for (c in unique(pannot$Chromosome)) {
    ix=pannot$Chromosome==c
    ix2=annot$Chromosome==c
    nsp=unique(c(pannot$nsp.start[ix],pannot$nsp.end[ix],annot$nsp.start[ix2],annot$nsp.end[ix2]))
    nsp=nsp[!is.na(nsp)]
    ix3=which(ix&na)
    for (i in ix3) {
        s[i]=max(nsp[nsp<p[i]],na.rm=T)
        e[i]=min(nsp[nsp>p[i]],na.rm=T)
    }
}
pannot$nsp.start=s
pannot$nsp.end=e
pannot$nsp.length=e-s


## Adda förenklat GC till annot: (flyttas till moddaA)
gc=rep(NA,nrow(pannot)) 
gc[!pannot$sty] <- pannot$nsp.gc[!pannot$sty]
gc[!pannot$nsp] <- pannot$sty.gc[!pannot$nsp]
gc[is.na(gc)] <- (pannot$nsp.gc[is.na(gc)]+pannot$sty.gc[is.na(gc)])/2
pannot$gc=gc



save(pannot,file=paste("data/CytoScanHD_pannot_",format(Sys.time(), "%F_%H_%M_%S"),".Rdata",sep=''))
save(pannot,file='data/pannot.Rdata')






