## Function(s) for segmentation
getPeaks <- function(t) { # which points in a vector are peaks
    return(c(F,diff(sign(diff(t)))<0,F))
}
smooth <- function(t,w=5) {
    s=t
    n=length(t)
    for (i in 1:n) {
        left=max(1,i-w+1):i
        right=(i+1):(min(n,i+w))
        s[i]=mean(t[c(left,i,right)])
    }
    return(s)
}
getBreaks <- function(data,w1,w2,pcut) {
    #w1=100
    #w2=100
    #pcut=3
    #data=c(rnorm(6000),rnorm(1000)+1,rnorm(3000))
    na=(is.na(data))
    data=(data[!na])
    n=length(data)
    p=rep(NA,n-1)
    for (i in 1:(n-1)) { # all possible breakpoints 
        left=max(1,i-w1+1):i
        right=(i+1):(min(n,i+w2))
        tx=wilcox.test(data[left],data[right])
        p[i]=tx$p.value[[1]]
    }
    p=-log10(p)
    plot(p) # p values are n-1, representing all breaks 1:2, 2:3.....
    peaks=getPeaks(p) & p>pcut
    # Keep only best peak in each window:
    for (i in which(peaks)[order(p[peaks],decreasing=T)]) {
        left=max(1,i-w1)
        right=min(length(p),i+w2)
        if (max(p[left:right])!=p[i]) peaks[i]=F
    }
    points(x=(1:length(peaks))[peaks],
           y=p[peaks],
           pch=2,col='red')
    
    out=rep(F,length(na)) # vector of length of orig. data
    out[!na][which(peaks)]=T
    return(which(out))
}
getSegments <- function(logr, lpos, baf, bpos) {
    
    breakpos1  <- breakpos2 <- NULL
    
    lpos=lpos[!is.na(logr)]
    logr=logr[!is.na(logr)]
    
    ## get breakpoint set from logr using different windows
    w1=c(100,50,10)
    w2=c(10,50,100)
    breaks=NULL
    for (i in 1:length(w1)) {
        breaks=c(breaks,getBreaks(logr,w1[i],w2[i],pcut=3))
    }
    breaks=sort(unique(breaks))
    
    ## Test if cuts are good ("undo")
    if (length(breaks)>0) {
        p=NULL; t=breaks
        for (i in 1:length(t)) {
            if (i==1) left=0 else left=t[i-1]
            if (i==length(t)) right=length(logr) else right=t[i+1]
            p[i]=wilcox.test(logr[(left+1):t[i]],logr[(t[i]+1):(right)])$p.value[[1]]
        } 
        p=-log10(p)
        # Remove bad peaks
        breaks=breaks[p>1]
    }
    if (length(breaks)>0) breakpos1 = (lpos[breaks]+lpos[breaks+1])/2
    
    ## get breakpoints from BAF using long windows
#     het=rnorm(1000)/10+0.5
#     hom1=abs(rnorm(1000))/10
#     hom2=1-abs(rnorm(1000))/10
#     baf=c(het, hom1, hom2)[order(rnorm(3000))]
#     het2=c(rnorm(500)/10+0.7,rnorm(500)/10+0.3, abs(rnorm(1000))/10,1-abs(rnorm(1000))/10)[order(rnorm(3000))]
#     het3=c(rnorm(500)/10+0.8,rnorm(500)/10+0.2, abs(rnorm(1000))/10,1-abs(rnorm(1000))/10)[order(rnorm(3000))]
#     baf=c(baf, het2, het3, baf)
#     
#     baf=abs(baf-.5); baf[baf>0.4]=NA ## may need to be adjusted
    bpos=bpos[!is.na(baf)]
    baf=baf[!is.na(baf)]
    
    w1=150
    w2=150
    breaks=NULL
    for (i in 1:length(w1)) {
        breaks=c(breaks,getBreaks(baf,w1[i],w2[i],pcut=3))
    }
    breaks=sort(unique(breaks))
    
    ## Test if BAF cuts are good ("undo")
    if (length(breaks)>0) {
        p=NULL; t=breaks
        for (i in 1:length(t)) {
            if (i==1) left=0 else left=t[i-1]
            if (i==length(t)) right=length(logr) else right=t[i+1]
            p[i]=wilcox.test(abs(baf-.5)[(left+1):t[i]],abs(baf-.5)[(t[i]+1):(right)])$p.value[[1]]
        } 
        p=-log10(p)
        breaks=breaks[p>2]
    }
    if (length(breaks)>0) breakpos2 = (bpos[breaks]+bpos[breaks+1])/2
    
    # prune BAF breakpoints, no nearer than 1MB to CN breaks
    for (i in length(breakpos2):1) {
        d=min(abs(breakpos2[i]-breakpos1))
        if (d<1e6) breakpos2 = breakpos2[-i]
    }
    breakpos=round(sort(c(breakpos1,breakpos2)))
    
    ## return segments
    Start=c(min(c(lpos,bpos)),breakpos)
    End=c(breakpos,max(c(lpos,bpos)))
    #Value=rep(NA,length(Start))
    #for (i in 1:length(Value)) Value[i]=median(logr[lpos>Start[i] & lpos<End[i]],na.rm=T)
    return(data.frame(Start,End))
}
